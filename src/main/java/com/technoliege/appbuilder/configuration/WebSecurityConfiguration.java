package com.technoliege.appbuilder.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.technoliege.appbuilder.security.JWTAuthenticationFilter;
import com.technoliege.appbuilder.security.JWTAuthorizationFilter;
import com.technoliege.appbuilder.security.JWTUtility;
import com.technoliege.appbuilder.security.UserSecurityDetailsService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserSecurityDetailsService userSecurityDetailsService;
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		http.csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().addFilter(getJWTAuthenticationFilter()).addFilter(new JWTAuthorizationFilter(authenticationManager(), jwtUtility()));
		super.configure(http);
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userSecurityDetailsService).passwordEncoder(encoder());
	}
	
	public JWTAuthenticationFilter getJWTAuthenticationFilter() throws Exception {

		final JWTAuthenticationFilter jwtAuthenticationFilter = new JWTAuthenticationFilter(authenticationManager(), jwtUtility());
		jwtAuthenticationFilter.setFilterProcessesUrl("/test");
		return jwtAuthenticationFilter;
	}
	
	@Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public JWTUtility jwtUtility() {
		return new JWTUtility();
	}
}
