package com.technoliege.appbuilder.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name="phase")
public class Phase extends AbstractEntity {

	private Organization organization;
	private Environment environment;
	private Application application;
	private WorkFlow workFlow;
	private Boolean canStartHere;
	private List<Phase> forwardTo;
	private List<Phase> backTo;
	private User defaultAssigned;
	private AssignmentType assignmentType;
	private Integer duration;
	private List<User>notifyOnEnter;
	private List<User>notifyOnExit;
	private List<User>notifyOnLateness;
	private Boolean canChangeDueDate;
	private List<User> notifyOnChangingDueDate;
	private String emailSubject;
	private String emailBody;
	private List<User> whoCanVoid;
	private List<User> notifyOnVoiding;
	
	@ManyToOne
	@JoinColumn(name = "organization", nullable = false)
	
	@JsonView({JsonSerializationViews.PhaseForm.class , JsonSerializationViews.PhaseView.class})
	public Organization getOrganization() {
		return organization;
	}
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	@ManyToOne
	@JoinColumn(name = "environment", nullable = false)
	@JsonView({JsonSerializationViews.PhaseForm.class , JsonSerializationViews.PhaseView.class})
	public Environment getEnvironment() {
		return environment;
	}
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	@ManyToOne
	@JoinColumn(name = "application", nullable = false)
	@JsonView({JsonSerializationViews.PhaseForm.class , JsonSerializationViews.PhaseView.class})
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application application) {
		this.application = application;
	}
	
	@ManyToOne
	@JoinColumn(name = "workFlow", nullable = false)
	@JsonView({JsonSerializationViews.PhaseForm.class , JsonSerializationViews.PhaseView.class})
	public WorkFlow getWorkFlow() {
		return workFlow;
	}
	public void setWorkFlow(WorkFlow workFlow) {
		this.workFlow = workFlow;
	}
	
	@Column(name="can_start_here",nullable = true)
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public Boolean getCanStartHere() {
		return canStartHere;
	}
	public void setCanStartHere(Boolean canStartHere) {
		this.canStartHere = canStartHere;
	}
	
	@ManyToMany
	@JoinTable(name = "forward_phase", joinColumns = { @JoinColumn(name = "phase_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "forward_phase_id", nullable = false) })
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public List<Phase> getForwardTo() {
		return forwardTo;
	}
	public void setForwardTo(List<Phase> forwardTo) {
		this.forwardTo = forwardTo;
	}
	
	@ManyToMany
	@JoinTable(name = "forward_phase", joinColumns = { @JoinColumn(name = "phase_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "back_phase_id", nullable = false) })
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public List<Phase> getBackTo() {
		return backTo;
	}
	public void setBackTo(List<Phase> backTo) {
		this.backTo = backTo;
	}
	
	@ManyToOne
	@JoinColumn(name = "default_assigned", nullable = false)
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public User getDefaultAssigned() {
		return defaultAssigned;
	}
	public void setDefaultAssigned(User defaultAssigned) {
		this.defaultAssigned = defaultAssigned;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="assignment_type",nullable = true)
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public AssignmentType getAssignmentType() {
		return assignmentType;
	}
	public void setAssignmentType(AssignmentType assignmentType) {
		this.assignmentType = assignmentType;
	}
	
	@Column(name="duration",nullable = true)
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	
	@ManyToMany
	@JoinTable(name = "Phase_Notified_On_Enter", joinColumns = { @JoinColumn(name = "phase_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false)})
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getNotifyOnEnter() {
		return notifyOnEnter;
	}
	public void setNotifyOnEnter(List<User> notifyOnEnter) {
		this.notifyOnEnter = notifyOnEnter;
	}
	
	@ManyToMany
	@JoinTable(name = "Phase_Notified_On_Exit", joinColumns = { @JoinColumn(name = "phase_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) })
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getNotifyOnExit() {
		return notifyOnExit;
	}
	public void setNotifyOnExit(List<User> notifyOnExit) {
		this.notifyOnExit = notifyOnExit;
	}

	@ManyToMany
	@JoinTable(name = "Phase_Notified_On_Lateness", joinColumns = { @JoinColumn(name = "phase_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) })
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getNotifyOnLateness() {
		return notifyOnLateness;
	}
	
	public void setNotifyOnLateness(List<User> notifyOnLateness) {
		this.notifyOnLateness = notifyOnLateness;
	}
	
	@Column(name = "can_change_due_date", nullable = true)
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public Boolean getCanChangeDueDate() {
		return canChangeDueDate;
	}
	public void setCanChangeDueDate(Boolean canChangeDueDate) {
		this.canChangeDueDate = canChangeDueDate;
	}
	
	@Column(name = "email_subject", length = 500, nullable = true, unique = false)
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public String getEmailSubject() {
		return emailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	
	@Column(name = "email_body", length = 500, nullable = true, unique = false)
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public String getEmailBody() {
		return emailBody;
	}
	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}
	
	@ManyToMany
	@JoinTable(name = "phase_void_users", joinColumns = { @JoinColumn(name = "phase_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) })
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getWhoCanVoid() {
		return whoCanVoid;
	}
	public void setWhoCanVoid(List<User> whoCanVoid) {
		this.whoCanVoid = whoCanVoid;
	}
	
	@ManyToMany
	@JoinTable(name = "phase_notify_on_void", joinColumns = { @JoinColumn(name = "phase_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) })
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getNotifyOnVoiding() {
		return notifyOnVoiding;
	}
	public void setNotifyOnVoiding(List<User> notifyOnVoiding) {
		this.notifyOnVoiding = notifyOnVoiding;
	}
	
	@ManyToMany
	@JoinTable(name = "phase_notify_on_change_duedate", joinColumns = { @JoinColumn(name = "phase_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) })
	@JsonView({JsonSerializationViews.PhaseForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getNotifyOnChangingDueDate() {
		return notifyOnChangingDueDate;
	}
	public void setNotifyOnChangingDueDate(List<User> notifyOnChangingDueDate) {
		this.notifyOnChangingDueDate = notifyOnChangingDueDate;
	}
	
	
}
