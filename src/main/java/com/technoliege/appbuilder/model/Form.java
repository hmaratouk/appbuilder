package com.technoliege.appbuilder.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name="form")
public class Form extends AbstractEntity{
	
	private Organization organization;
	private Environment environment;
	private Application application;
	private List<Field> fields;
	private String tableName;
	private Integer gridVerticals;
	private Integer gridHorizontal;
	private List<User> managers;
	private List<User> authers;
	private List<User> readers;
	private String numberingPrefix;
	private String numberingPostfix;
	
	@ManyToOne
	@JoinColumn(name = "organization", nullable = false)
	@JsonView({JsonSerializationViews.FormForm.class, JsonSerializationViews.FormView.class})
	public Organization getOrganization() {
		return organization;
	}
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	@ManyToOne
	@JoinColumn(name = "environment", nullable = false)
	@JsonView({JsonSerializationViews.FormForm.class, JsonSerializationViews.FormView.class})
	public Environment getEnvironment() {
		return environment;
	}
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	@ManyToOne
	@JoinColumn(name = "application", nullable = false)
	@JsonView({JsonSerializationViews.FormForm.class, JsonSerializationViews.FormView.class})
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application app) {
		this.application = app;
	}
	
	@OneToMany(mappedBy = "form")
	@JsonView({JsonSerializationViews.FormForm.class ,JsonSerializationViews.metadata.class})
	public List<Field> getFields() {
		return fields;
	}
	public void setFields(List<Field> fields) {
		this.fields = fields;
	}
	
	@Column(name = "table_name", length = 150, nullable = false)
	@JsonView({JsonSerializationViews.FormForm.class ,JsonSerializationViews.metadata.class})
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	@Column(name = "grid_verticals", nullable = false)
	@JsonView({JsonSerializationViews.FormForm.class, JsonSerializationViews.metadata.class})
	public Integer getGridVerticals() {
		return gridVerticals;
	}
	
	public void setGridVerticals(Integer gridVerticals) {
		this.gridVerticals = gridVerticals;
	}
	
	@Column(name = "grid_horizontal", nullable = false)
	@JsonView({JsonSerializationViews.FormForm.class ,JsonSerializationViews.metadata.class})
	public Integer getGridHorizontal() {
		return gridHorizontal;
	}
	public void setGridHorizontal(Integer gridHorizontal) {
		this.gridHorizontal = gridHorizontal;
	}
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "form_manager", joinColumns = { @JoinColumn(name = "form_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "manager_id", nullable = false) })
	@JsonView({JsonSerializationViews.FormForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getManagers() {
		return managers;
	}
	public void setManagers(List<User> managers) {
		this.managers = managers;
	}
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "form_auther", joinColumns = { @JoinColumn(name = "form_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "auther_id", nullable = false) })
	@JsonView({JsonSerializationViews.FormForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getAuthers() {
		return authers;
	}
	public void setAuthers(List<User> authers) {
		this.authers = authers;
	}
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "form_reader", joinColumns = { @JoinColumn(name = "form_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "reader_id", nullable = false) })
	@JsonView({JsonSerializationViews.FormForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getReaders() {
		return readers;
	}
	public void setReaders(List<User> readers) {
		this.readers = readers;
	}
	
	@Column(name = "numbering_prefix", nullable = true)
	@JsonView({JsonSerializationViews.FormForm.class ,JsonSerializationViews.metadata.class})
	public String getNumberingPrefix() {
		return numberingPrefix;
	}
	public void setNumberingPrefix(String numberingPrefix) {
		this.numberingPrefix = numberingPrefix;
	}
	
	@Column(name = "numbering_postfix", nullable = true)
	@JsonView({JsonSerializationViews.FormForm.class ,JsonSerializationViews.metadata.class})
	public String getNumberingPostfix() {
		return numberingPostfix;
	}
	public void setNumberingPostfix(String numberingPostfix) {
		this.numberingPostfix = numberingPostfix;
	}

}
