package com.technoliege.appbuilder.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name="organization")
public class Organization extends AbstractEntity {

	//Direct children components
	private List<Environment> environments = new ArrayList<Environment>();
	
	//Visual properties
	private String logo;
	
	//Access Control Properties
	private User superAdmin;
	private List<User> usersCreators = new ArrayList<User>();
	
	//Functionality properties
	private Integer maxNumberOfUsers;
	private Boolean canImportApplications;
	private Boolean canExportApplications;
	private Boolean canCreateApplications;
	
	@OneToMany(mappedBy = "organization", fetch = FetchType.LAZY, orphanRemoval = true)
	@JsonView({ JsonSerializationViews.OrganizationForm.class, JsonSerializationViews.metadata.class })
	public List<Environment> getEnvironments() {
		return environments;
	}
	
	public void setEnvironments(List<Environment> environments) {
		this.environments = environments;
	}
	
	@Column(name = "logo", length = 500)
	@JsonView({JsonSerializationViews.OrganizationForm.class ,JsonSerializationViews.metadata.class})
	public String getLogo() {
		return logo;
	}
	
	public void setLogo(String logo) {
		this.logo = logo;
	}
	
	@OneToOne
	@JoinColumn(name = "super_admin", nullable = false)
	@JsonView({JsonSerializationViews.OrganizationForm.class ,JsonSerializationViews.metadata.class})
	public User getSuperAdmin() {
		return superAdmin;
	}
	
	public void setSuperAdmin(User superAdmin) {
		this.superAdmin = superAdmin;
	}
	
	@OneToMany
	//@JoinColumn(name = "users_creators")
	@JsonView({JsonSerializationViews.OrganizationForm.class })
	public List<User> getUsersCreators() {
		return usersCreators;
	}
	
	public void setUsersCreators(List<User> usersCreators) {
		this.usersCreators = usersCreators;
	}
	
	@Column(name = "max_number_of_users", nullable = false)
	@JsonView({JsonSerializationViews.OrganizationForm.class })
	public Integer getMaxNumberOfUsers() {
		return maxNumberOfUsers;
	}
	
	public void setMaxNumberOfUsers(Integer maxNumberOfUsers) {
		this.maxNumberOfUsers = maxNumberOfUsers;
	}
	
	@Column(name = "can_import_applications")
	@JsonView({JsonSerializationViews.OrganizationForm.class ,JsonSerializationViews.metadata.class})
	public Boolean getCanImportApplications() {
		return canImportApplications;
	}
	
	public void setCanImportApplications(Boolean canImportApplications) {
		this.canImportApplications = canImportApplications;
	}
	
	@Column(name = "can_export_applications")
	@JsonView({JsonSerializationViews.OrganizationForm.class ,JsonSerializationViews.metadata.class})
	public Boolean getCanExportApplications() {
		return canExportApplications;
	}
	
	public void setCanExportApplications(Boolean canExportApplications) {
		this.canExportApplications = canExportApplications;
	}
	
	@Column(name = "can_create_applications")
	@JsonView({JsonSerializationViews.OrganizationForm.class })
	public Boolean getCanCreateApplications() {
		return canCreateApplications;
	}
	
	public void setCanCreateApplications(Boolean canCreateApplications) {
		this.canCreateApplications = canCreateApplications;
	}
	
}
