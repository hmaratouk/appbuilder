package com.technoliege.appbuilder.model;

public final class EntitesTypseIDs {

	final static int ORGANIZATION_ID = 1;
	final static int ENVIRONMENT_ID = 2;
	final static int APPLICATION_ID = 3;
	final static int FORM_ID = 4;
	final static int VIEW_ID = 5;
	final static int WORKFLOW_ID = 6;
	final static int COLUMN_ID = 7;
	final static int PHASE_ID = 8;
	final static int FIELD_ID = 9;
	final static int USER_ID = 9;
}
