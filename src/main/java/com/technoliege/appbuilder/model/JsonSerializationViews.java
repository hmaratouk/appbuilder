package com.technoliege.appbuilder.model;

public class JsonSerializationViews {

	public static class AbstractView {
	}

	public static class ApplicationView extends AbstractView {
	}

	public static class ApplicationForm extends ApplicationView {
	}

	public static class EnvironmentView extends AbstractView {
	}

	public static class EnvironmentForm extends EnvironmentView {
	}

	public static class FieldView extends AbstractView {
	}

	public static class FieldForm extends FieldView {
	}

	public static class FormView extends AbstractView {
	}

	public static class FormForm extends FormView {
	}

	public static class OrganizationView extends AbstractView {
	}

	public static class OrganizationForm extends OrganizationView {
	}

	public static class PhaseView extends AbstractView {
	}

	public static class PhaseForm extends PhaseView {
	}

	public static class UserView extends AbstractView {
	}

	public static class UserForm extends UserView {
	}

	public static class ViewView extends AbstractView {
	}

	public static class ViewForm extends ViewView {
	}

	public static class ViewColumnView extends AbstractView {
	}

	public static class ViewColumnForm extends ViewColumnView {
	}

	public static class WorkflowView extends AbstractView {
	}

	public static class WorkflowForm extends WorkflowView {
	}

	public static class loginForm {
	}
	
	public static class metadata extends AbstractView {
		
	}
	
}
