package com.technoliege.appbuilder.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name="viewColumn")
public class ViewColumn extends AbstractEntity {

	// Parent components
	private Organization organization;
	private Environment environment;
	private Application application;
	private View view;
	
	private Field field;
	
	//Visual properties
	private Integer orderInParent;
	
	//Functionality
	private Boolean storable;
	private Boolean searchable;
	private Integer numberOfCharactersToDisplay;
	private DateTimeFormat dateTimeFormat;
	private Integer decimalPlaces;
	
	
	@ManyToOne
	@JoinColumn(name = "organization", nullable = false)
	@JsonView({JsonSerializationViews.ViewColumnForm.class , JsonSerializationViews.ViewColumnView.class})
	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@ManyToOne
	@JoinColumn(name = "environment", nullable = false)
	@JsonView({JsonSerializationViews.ViewColumnForm.class , JsonSerializationViews.ViewColumnView.class})
	public Environment getEnvironment() {
		return environment;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	@ManyToOne
	@JoinColumn(name = "application", nullable = false)
	@JsonView({JsonSerializationViews.ViewColumnForm.class , JsonSerializationViews.ViewColumnView.class})
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	@ManyToOne
	@JoinColumn(name = "view", nullable = false)
	@JsonView({JsonSerializationViews.ViewColumnForm.class , JsonSerializationViews.ViewColumnView.class })
	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	@OneToOne
	@JoinColumn(name = "field")
	@JsonView({JsonSerializationViews.ViewColumnForm.class , JsonSerializationViews.ViewColumnView.class ,JsonSerializationViews.metadata.class})
	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	@Column(name = "order_in_parent")
	@JsonView({JsonSerializationViews.ViewColumnForm.class ,JsonSerializationViews.metadata.class})
	public Integer getOrderInParent() {
		return orderInParent;
	}

	public void setOrderInParent(Integer orderInParent) {
		this.orderInParent = orderInParent;
	}

	@Column(name = "storable")
	@JsonView({JsonSerializationViews.ViewColumnForm.class ,JsonSerializationViews.metadata.class})
	public Boolean getStorable() {
		return storable;
	}

	public void setStorable(Boolean storable) {
		this.storable = storable;
	}

	@Column(name = "searchable")
	@JsonView({JsonSerializationViews.ViewColumnForm.class ,JsonSerializationViews.metadata.class})
	public Boolean getSearchable() {
		return searchable;
	}

	public void setSearchable(Boolean searchable) {
		this.searchable = searchable;
	}

	@Column(name = "number_of_characters_to_display")
	@JsonView({JsonSerializationViews.ViewColumnForm.class ,JsonSerializationViews.metadata.class})
	public Integer getNumberOfCharactersToDisplay() {
		return numberOfCharactersToDisplay;
	}

	public void setNumberOfCharactersToDisplay(Integer numberOfCharactersToDisplay) {
		this.numberOfCharactersToDisplay = numberOfCharactersToDisplay;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "date_time_format", length = 100)
	@JsonView({JsonSerializationViews.ViewColumnForm.class ,JsonSerializationViews.metadata.class})
	public DateTimeFormat getDateTimeFormat() {
		return dateTimeFormat;
	}

	public void setDateTimeFormat(DateTimeFormat dateTimeFormat) {
		this.dateTimeFormat = dateTimeFormat;
	}

	@Column(name = "decimal_places")
	@JsonView({JsonSerializationViews.ViewColumnForm.class ,JsonSerializationViews.metadata.class})
	public Integer getDecimalPlaces() {
		return decimalPlaces;
	}

	public void setDecimalPlaces(Integer decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}
	
}
