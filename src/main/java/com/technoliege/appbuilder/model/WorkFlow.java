package com.technoliege.appbuilder.model;

import java.util.Date;
import java.util.List;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Table(name="workflow")
public class WorkFlow extends AbstractEntity {
	private Organization organization;
	private Environment environment;
	private Application application;
	private Form form;
	private List<Phase> phases;
	private List<User>authers;
	private Boolean revisionControled;
	
	private List<User>whoCanInitiate;
	private List<User>whoCanInitiateChangeRequest;
	private List<User>notifyOnNewRevision;
	
	@ManyToOne
	@JoinColumn(name = "organization", nullable = false)
	@JsonView({JsonSerializationViews.WorkflowForm.class , JsonSerializationViews.WorkflowView.class})
	public Organization getOrganization() {
		return organization;
	}
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	@ManyToOne
	@JoinColumn(name = "environment", nullable = false)
	@JsonView({JsonSerializationViews.WorkflowForm.class , JsonSerializationViews.WorkflowView.class})
	public Environment getEnvironment() {
		return environment;
	}
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	@ManyToOne
	@JoinColumn(name = "application", nullable = false)
	@JsonView({JsonSerializationViews.WorkflowForm.class , JsonSerializationViews.WorkflowView.class})
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application app) {
		this.application = app;
	}
	
	@ManyToOne
	@JoinColumn(name = "form", nullable = false)
	@JsonView({JsonSerializationViews.WorkflowForm.class , JsonSerializationViews.WorkflowView.class ,JsonSerializationViews.metadata.class})
	public Form getForm() {
		return form;
	}
	public void setForm(Form form) {
		this.form = form;
	}
	
	@OneToMany(mappedBy="workFlow", fetch = FetchType.LAZY, orphanRemoval = true)
	@JsonView({JsonSerializationViews.WorkflowForm.class ,JsonSerializationViews.metadata.class})
	public List<Phase> getPhases() {
		return phases;
	}
	public void setPhases(List<Phase> phases) {
		this.phases = phases;
	}
	
	@ManyToMany
	@JoinTable(name = "workflow_auther", joinColumns = { @JoinColumn(name = "workflow_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "auther_id", nullable = false) })
	@JsonView({JsonSerializationViews.WorkflowForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getAuthers() {
		return authers;
	}
	public void setAuthers(List<User> authers) {
		this.authers = authers;
	}
	
	@Column(name = "revision_controled", nullable = true, unique = false, columnDefinition = "boolean default false")
	@JsonView({JsonSerializationViews.WorkflowForm.class ,JsonSerializationViews.metadata.class})
	public Boolean getRevisionControled() {
		return revisionControled;
	}
	public void setRevisionControled(Boolean revisionControled) {
		this.revisionControled = revisionControled;
	}
	
	@ManyToMany
	@JoinTable(name = "Workflow_Initiaters", joinColumns = { @JoinColumn(name = "workflow_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) })
	@JsonView({JsonSerializationViews.WorkflowForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getWhoCanInitiate() {
		return whoCanInitiate;
	}
	public void setWhoCanInitiate(List<User> whoCanInitiate) {
		this.whoCanInitiate = whoCanInitiate;
	}
	
	@ManyToMany
	@JoinTable(name = "Workflow_change_request_Initiaters", joinColumns = { @JoinColumn(name = "workflow_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) })
	@JsonView({JsonSerializationViews.WorkflowForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getWhoCanInitiateChangeRequest() {
		return whoCanInitiateChangeRequest;
	}
	public void setWhoCanInitiateChangeRequest(List<User> whoCanInitiateChangeRequest) {
		this.whoCanInitiateChangeRequest = whoCanInitiateChangeRequest;
	}
	
	@ManyToMany
	@JoinTable(name = "Workflow_notify_on_revision", joinColumns = { @JoinColumn(name = "workflow_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) })
	@JsonView({JsonSerializationViews.WorkflowForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getNotifyOnNewRevision() {
		return notifyOnNewRevision;
	}
	public void setNotifyOnNewRevision(List<User> notifyOnNewRevision) {
		this.notifyOnNewRevision = notifyOnNewRevision;
	}
}
