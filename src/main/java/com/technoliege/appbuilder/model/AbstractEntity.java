package com.technoliege.appbuilder.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonView;

@MappedSuperclass
@EntityListeners({AuditingEntityListener.class})
public class AbstractEntity {

	//System properties
	private Integer id;
	private Integer typeID;
	private String universalID;
	private User createdBy;
	private Date createdOn;
	private User lastEditedBy;
	private Date lastEditedOn;
	
	
	//Basic properties
	private String displayName;
	private String description;
	private Boolean disabled;
	
	@JsonView({JsonSerializationViews.AbstractView.class, JsonSerializationViews.loginForm.class})
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	@JsonView({JsonSerializationViews.AbstractView.class})
	@Column(name = "universal_id")
	public String getUniversalID() {
		return universalID;
	}

	public void setUniversalID(String universalID) {
		this.universalID = universalID;
	}

	@JsonView({JsonSerializationViews.AbstractView.class})
	@ManyToOne
	@JoinColumn(name = "created_by", nullable = false, updatable = false)
	//@CreatedBy
	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	@JsonView({JsonSerializationViews.AbstractView.class})
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_on", nullable = false, updatable = false)
	@CreatedDate
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@JsonView({JsonSerializationViews.AbstractView.class})
	@ManyToOne
	//@LastModifiedBy
	@JoinColumn(name = "last_edited_by", nullable = false, updatable = false)
	public User getLastEditedBy() {
		return lastEditedBy;
	}

	public void setLastEditedBy(User lastEditedBy) {
		this.lastEditedBy = lastEditedBy;
	}

	@JsonView({JsonSerializationViews.AbstractView.class})
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_edited_on", nullable = false)
	@LastModifiedDate
	public Date getLastEditedOn() {
		return lastEditedOn;
	}

	public void setLastEditedOn(Date lastEditedOn) {
		this.lastEditedOn = lastEditedOn;
	}

	@JsonView({JsonSerializationViews.AbstractView.class , JsonSerializationViews.loginForm.class})
	@Column(name = "display_name", length = 150, nullable = false)
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@JsonView({JsonSerializationViews.AbstractView.class})
	@Column(name = "description", length = 500, nullable = true)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonView({JsonSerializationViews.AbstractView.class})
	@Column(name = "disabled")
	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractEntity other = (AbstractEntity) obj;
		
		if (getUniversalID() == null) {
			if (other.getUniversalID() != null)
				return false;
		} else if (!getUniversalID().equals(other.getUniversalID()))
			return false;
		
		return true;
	}
}
