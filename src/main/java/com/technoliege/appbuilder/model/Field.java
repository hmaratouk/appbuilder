package com.technoliege.appbuilder.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name="field")
public class Field extends AbstractEntity {

	// Parent components
	private Organization organization;
	private Environment environment;
	private Application application;
	private Form form;
	private Boolean multiValue;
	
	
	private String documentColumnName;
	private String multiValueColumnName;
	
	
	//Database properties
	//holds either the name of the field column name (in case it is a single value field) or the field table name (in case it is a multi value field)
	private String databasePropertyName; 
	
	//Visual properties
	private Integer lineNumber;
	private Integer columnNumber;
	private Integer width;
	
	//Functionality properties
	private FieldType fieldType;
	private EditabilityStyle editabilityStyle;
	private Boolean isRequired;
	private String defaultValue;
	private Boolean execludedFromIndexing;
	private Boolean execludeFromPersisting;

	@ManyToOne
	@JoinColumn(name = "organization", nullable = false)
	@JsonView({JsonSerializationViews.FieldForm.class, JsonSerializationViews.FieldView.class})
	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@ManyToOne
	@JoinColumn(name = "environment", nullable = false)
	@JsonView({JsonSerializationViews.FieldForm.class, JsonSerializationViews.FieldView.class})
	public Environment getEnvironment() {
		return environment;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	@ManyToOne
	@JoinColumn(name = "application", nullable = false)
	@JsonView({JsonSerializationViews.FieldForm.class, JsonSerializationViews.FieldView.class})
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	@ManyToOne
	@JoinColumn(name = "form", nullable = false)
	@JsonView({JsonSerializationViews.FieldForm.class, JsonSerializationViews.FieldView.class})
	public Form getForm() {
		return form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

	@Column(name = "database_property_name")
	@JsonView({JsonSerializationViews.FieldForm.class ,JsonSerializationViews.metadata.class})
	public String getDatabasePropertyName() {
		return databasePropertyName;
	}

	public void setDatabasePropertyName(String databasePropertyName) {
		this.databasePropertyName = databasePropertyName;
	}

	

	@Column(name = "line_number", nullable = false)
	@JsonView({JsonSerializationViews.FieldForm.class ,JsonSerializationViews.metadata.class})
	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	@Column(name = "column_number", nullable = false)
	@JsonView({JsonSerializationViews.FieldForm.class ,JsonSerializationViews.metadata.class})
	public Integer getColumnNumber() {
		return columnNumber;
	}

	public void setColumnNumber(Integer columnNumber) {
		this.columnNumber = columnNumber;
	}

	@Column(name = "width")
	@JsonView({JsonSerializationViews.FieldForm.class ,JsonSerializationViews.metadata.class})
	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "type", length = 100)
	@JsonView({JsonSerializationViews.FieldForm.class ,JsonSerializationViews.metadata.class})
	public FieldType getFieldType() {
		return fieldType;
	}

	public void setFieldType(FieldType fieldType) {
		this.fieldType = fieldType;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "editablity_style", length = 100)
	@JsonView({JsonSerializationViews.FieldForm.class ,JsonSerializationViews.metadata.class})
	public EditabilityStyle getEditabilityStyle() {
		return editabilityStyle;
	}

	public void setEditabilityStyle(EditabilityStyle editabilityStyle) {
		this.editabilityStyle = editabilityStyle;
	}

	@Column(name = "is_required")
	@JsonView({JsonSerializationViews.FieldForm.class ,JsonSerializationViews.metadata.class})
	public Boolean getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(Boolean isRequired) {
		this.isRequired = isRequired;
	}

	@Column(name = "default_value")
	@JsonView({JsonSerializationViews.FieldForm.class ,JsonSerializationViews.metadata.class})
	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	@Column(name = "execluded_from_indexing")
	@JsonView({JsonSerializationViews.FieldForm.class ,JsonSerializationViews.metadata.class})
	public Boolean getExecludedFromIndexing() {
		return execludedFromIndexing;
	}

	public void setExecludedFromIndexing(Boolean execludedFromIndexing) {
		this.execludedFromIndexing = execludedFromIndexing;
	}

	@Column(name = "execluded_from_persisting")
	@JsonView({JsonSerializationViews.FieldForm.class ,JsonSerializationViews.metadata.class})
	public Boolean getExecludeFromPersisting() {
		return execludeFromPersisting;
	}

	public void setExecludeFromPersisting(Boolean execludeFromPersisting) {
		this.execludeFromPersisting = execludeFromPersisting;
	}

	
	@Column(name="multi_value", nullable = true)
	@JsonView({JsonSerializationViews.metadata.class})
	public Boolean getMultiValue() {
		return multiValue;
	}

	public void setMultiValue(Boolean multiValue) {
		this.multiValue = multiValue;
	}

	@JsonView({JsonSerializationViews.metadata.class})
	public String getDocumentColumnName() {
		return documentColumnName;
	}

	public void setDocumentColumnName(String documentColumnName) {
		this.documentColumnName = documentColumnName;
	}

	@JsonView({JsonSerializationViews.metadata.class})
	public String getMultiValueColumnName() {
		return multiValueColumnName;
	}

	public void setMultiValueColumnName(String multiValueColumnName) {
		this.multiValueColumnName = multiValueColumnName;
	}
	
}
