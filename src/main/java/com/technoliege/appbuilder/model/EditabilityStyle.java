package com.technoliege.appbuilder.model;

public enum EditabilityStyle {

	EDITABLE,READ_ONLY;
}
