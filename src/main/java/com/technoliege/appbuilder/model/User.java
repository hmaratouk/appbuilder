package com.technoliege.appbuilder.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name="users")
@JsonIgnoreProperties(value = { "createdBy", "lastEditedBy" }, allowSetters = true)
public class User extends AbstractEntity{

	private String username;
	private String orgUserName;
	private String password;
	private boolean disabled;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private Organization organization;
	private UserRole userRole;
	
	@Column(name="user_name")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name="org_user_name")
	@JsonView({JsonSerializationViews.UserForm.class,JsonSerializationViews.UserView.class})
	public String getOrgUserName() {
		return orgUserName;
	}

	public void setOrgUserName(String orgUserName) {
		this.orgUserName = orgUserName;
	}

	@Column(name="password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name="is_disabled")
	@JsonView({JsonSerializationViews.UserForm.class})
	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	@Column(name="first_name")
	@JsonView({JsonSerializationViews.UserForm.class})
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name="last_name")
	@JsonView({JsonSerializationViews.UserForm.class})
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name="email")
	@JsonView({JsonSerializationViews.UserForm.class})
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "organization", nullable = false, updatable = false)
	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "user_role", length = 100)
	@JsonView({JsonSerializationViews.UserForm.class})
	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
}
