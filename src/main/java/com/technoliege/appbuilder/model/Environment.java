package com.technoliege.appbuilder.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;

import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Table(name="environment")
public class Environment extends AbstractEntity {

	private Organization organization;
	private List<Application> applications;
	private ThemeMarker themeMaker;
	private User owner;
//	public Environment promotesTo;
//	public Environment promotesFrom;
	
	@ManyToOne
	@JoinColumn(name = "organization", nullable = false, updatable = false)
	@JsonView({JsonSerializationViews.EnvironmentForm.class, JsonSerializationViews.EnvironmentView.class})
	public Organization getOrganization() {
		return organization;
	}
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	@OneToMany(mappedBy = "environment", fetch = FetchType.LAZY, orphanRemoval = true)
	@JsonView({JsonSerializationViews.EnvironmentForm.class ,JsonSerializationViews.metadata.class})
	public List<Application> getApplications() {
		return applications;
	}
	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "theme_maker", length = 100, nullable = false)
	@JsonView({JsonSerializationViews.EnvironmentForm.class ,JsonSerializationViews.metadata.class})
	public ThemeMarker getThemeMaker() {
		return themeMaker;
	}
	public void setThemeMaker(ThemeMarker themeMaker) {
		this.themeMaker = themeMaker;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "owner_id", nullable = true, updatable = false)
	@CreatedBy
	@JsonView({JsonSerializationViews.EnvironmentForm.class ,JsonSerializationViews.metadata.class})
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	
//	@Column(name="promotes_to")
//	public Environment getPromotesTo() {
//		return promotesTo;
//	}
//	public void setPromotesTo(Environment promotesTo) {
//		this.promotesTo = promotesTo;
//	}
//	
//	@Column(name="promotes_from")
//	public Environment getPromotesFrom() {
//		return promotesFrom;
//	}
//	public void setPromotesFrom(Environment promotesFrom) {
//		this.promotesFrom = promotesFrom;
//	}

	
}
