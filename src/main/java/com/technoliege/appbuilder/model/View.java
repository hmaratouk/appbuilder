package com.technoliege.appbuilder.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name="view")
public class View extends AbstractEntity {
	
	


	//Parent components
	private Organization organization;
	private Environment environment;
	private Application application;
	
	private Form form;
	
	//Direct children components
	private List<ViewColumn> viewColumns = new ArrayList<ViewColumn>();
	
	//Access control properties
	private List<User> viewers = new ArrayList<User>();
	
	//Functionality properties
	private Integer defaultPageSize;
	private OrderType defaultOrderType;
	
	@ManyToOne
	@JoinColumn(name = "organization", nullable = false)
	@JsonView({JsonSerializationViews.ViewForm.class , JsonSerializationViews.ViewView.class })
	public Organization getOrganization() {
		return organization;
	}
	
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	@ManyToOne
	@JoinColumn(name = "environment", nullable = false)
	@JsonView({JsonSerializationViews.ViewForm.class , JsonSerializationViews.ViewView.class })
	public Environment getEnvironment() {
		return environment;
	}
	
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	@ManyToOne
	@JoinColumn(name = "application", nullable = false)
	@JsonView({JsonSerializationViews.ViewForm.class , JsonSerializationViews.ViewView.class })
	public Application getApplication() {
		return application;
	}
	
	public void setApplication(Application application) {
		this.application = application;
	}
	
	@OneToOne
	@JoinColumn(name = "form", nullable = false)
	@JsonView({JsonSerializationViews.ViewForm.class , JsonSerializationViews.ViewView.class ,JsonSerializationViews.metadata.class})
	public Form getForm() {
		return form;
	}
	
	public void setForm(Form form) {
		this.form = form;
	}
	
	@OneToMany(mappedBy = "view", orphanRemoval = true)
	@JsonView({JsonSerializationViews.ViewForm.class ,JsonSerializationViews.metadata.class})
	public List<ViewColumn> getViewColumns() {
		return viewColumns;
	}
	
	public void setViewColumns(List<ViewColumn> viewColumns) {
		this.viewColumns = viewColumns;
	}
	
	@ManyToMany
	@JoinTable(name = "view_viewers", joinColumns = { @JoinColumn(name = "view_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) }, uniqueConstraints = @UniqueConstraint(columnNames = { "view_id", "user_id" }))
	@JsonView({JsonSerializationViews.ViewForm.class})
	public List<User> getViewers() {
		return viewers;
	}
	
	public void setViewers(List<User> viewers) {
		this.viewers = viewers;
	}
	
	@Column(name = "default_page_size", nullable = false)
	@JsonView({JsonSerializationViews.ViewForm.class ,JsonSerializationViews.metadata.class})
	public Integer getDefaultPageSize() {
		return defaultPageSize;
	}
	
	public void setDefaultPageSize(Integer defaultPageSize) {
		this.defaultPageSize = defaultPageSize;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "default_order_type", length = 100)
	@JsonView({JsonSerializationViews.ViewForm.class ,JsonSerializationViews.metadata.class})
	public OrderType getDefaultOrderType() {
		return defaultOrderType;
	}
	
	public void setDefaultOrderType(OrderType defaultOrderType) {
		this.defaultOrderType = defaultOrderType;
	}
	
		
}
