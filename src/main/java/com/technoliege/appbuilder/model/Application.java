package com.technoliege.appbuilder.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "application")
public class Application extends AbstractEntity {

	//Parent components
	private Organization organization;
	private Environment environment;
	
	//Direct children components
	private List<Form> forms = new ArrayList<Form>();
	private List<View> views = new ArrayList<View>();
	private List<WorkFlow> workflows = new ArrayList<WorkFlow>();
	
	//Database properties
	private String applicationSchema;
	
	//Visual properties
	private String icon;
	private Theme theme;
	
	//Access control properties
	private List<User> applicationManagers = new ArrayList<User>();
	private List<User> applicationUsers = new ArrayList<User>();
	private List<User> applicationReaders = new ArrayList<User>();
	
	
	//Functionality
	private View defaultView;

	@ManyToOne
	@JoinColumn(name = "organization", nullable = false)
	@JsonView({JsonSerializationViews.ApplicationForm.class})
	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	
	@ManyToOne
	@JoinColumn(name = "environment", nullable = false)
	@JsonView({JsonSerializationViews.ApplicationForm.class})
	public Environment getEnvironment() {
		return environment;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	@OneToMany(mappedBy="application", fetch = FetchType.LAZY, orphanRemoval = true)
	@JsonView({JsonSerializationViews.ApplicationForm.class ,JsonSerializationViews.metadata.class})
	public List<Form> getForms() {
		return forms;
	}

	public void setForms(List<Form> forms) {
		this.forms = forms;
	}

	@OneToMany(mappedBy="application", fetch = FetchType.LAZY, orphanRemoval = true)
	@JsonView({JsonSerializationViews.ApplicationForm.class ,JsonSerializationViews.metadata.class})
	public List<View> getViews() {
		return views;
	}

	public void setViews(List<View> views) {
		this.views = views;
	}

	@OneToMany(mappedBy="application", fetch = FetchType.LAZY, orphanRemoval = true)
	@JsonView({JsonSerializationViews.ApplicationForm.class ,JsonSerializationViews.metadata.class})
	public List<WorkFlow> getWorkflows() {
		return workflows;
	}

	public void setWorkflows(List<WorkFlow> workflows) {
		this.workflows = workflows;
	}

	@Column(name = "applicationSchema", length = 500)
	@JsonView({JsonSerializationViews.ApplicationForm.class ,JsonSerializationViews.metadata.class})
	public String getApplicationSchema() {
		return applicationSchema;
	}

	public void setApplicationSchema(String schema) {
		this.applicationSchema = schema;
	}

	@Column(name = "icon", length = 500)
	@JsonView({JsonSerializationViews.ApplicationForm.class ,JsonSerializationViews.metadata.class})
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "theme", length = 100)
	@JsonView({JsonSerializationViews.ApplicationForm.class ,JsonSerializationViews.metadata.class})
	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}
	
	@ManyToMany
	@JoinTable(name = "application_managers", joinColumns = { @JoinColumn(name = "application_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) })
	@JsonView({JsonSerializationViews.ApplicationForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getApplicationManagers() {
		return applicationManagers;
	}

	public void setApplicationManagers(List<User> applicationManagers) {
		this.applicationManagers = applicationManagers;
	}

	@ManyToMany
	@JoinTable(name = "application_users", joinColumns = { @JoinColumn(name = "application_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) })
	@JsonView({JsonSerializationViews.ApplicationForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getApplicationUsers() {
		return applicationUsers;
	}

	public void setApplicationUsers(List<User> applicationUsers) {
		this.applicationUsers = applicationUsers;
	}

	@ManyToMany
	@JoinTable(name = "application_readers", joinColumns = { @JoinColumn(name = "application_id", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false) })
	@JsonView({JsonSerializationViews.ApplicationForm.class ,JsonSerializationViews.metadata.class})
	public List<User> getApplicationReaders() {
		return applicationReaders;
	}

	public void setApplicationReaders(List<User> applicationReaders) {
		this.applicationReaders = applicationReaders;
	}

	@OneToOne
	@JoinColumn(name = "default_view")
	@JsonView({JsonSerializationViews.ApplicationForm.class ,JsonSerializationViews.metadata.class})
	public View getDefaultView() {
		return defaultView;
	}

	public void setDefaultView(View defaultView) {
		this.defaultView = defaultView;
	}
	
}
