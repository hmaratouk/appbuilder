package com.technoliege.appbuilder.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException.BadRequest;

import com.technoliege.appbuilder.model.Application;
import com.technoliege.appbuilder.model.Field;
import com.technoliege.appbuilder.model.Form;
import com.technoliege.appbuilder.model.Phase;

import com.technoliege.appbuilder.model.View;
import com.technoliege.appbuilder.repository.ApplicationRepository;
import com.technoliege.appbuilder.repository.FieldRepository;
import com.technoliege.appbuilder.repository.FormRepository;

@Service
public class FormBusiness extends AbstractBusiness<Form> {

	@Autowired
	FieldRepository fieldRepository;
	
	public FormBusiness(FormRepository repository) {
		this.repository = repository;
	}

	public List<Form> getAppForms(Integer applicationId) {
		return ((FormRepository) repository).findByApplicationId(applicationId);
	}

	@Override
	public void saveDocument(Form document) throws BusinessException {
		// TODO Auto-generated method stub
		// check if the display name is unique
		// if(!checkDisplayName(document.getDisplayName(), document.getApplication() ) )
		// throw new BusinessException("");

		document.setTableName(generateTableName(document.getDisplayName(), document.getApplication()));

		super.saveDocument(document);
		document.setUniversalID( generateUniversalId(document, document.getOrganization().getId(), "09"));
		super.saveDocument(document);
	}

	boolean checkDisplayName(String displayName, Application app) {
		Form form = ((FormRepository) repository).findByDisplayNameAndApplication(displayName, app);
		if (form == null)
			return true;

		return false;
	}

	String generateTableName(String formDisplayName, Application app) {

		String tableName = "";

		Pattern pt = Pattern.compile("[^a-zA-Z0-9_ ]");
		Matcher match = pt.matcher(formDisplayName);
		while (match.find()) {
			String s = match.group();
			formDisplayName = formDisplayName.replaceAll("\\" + s, "");
		}
		System.out.println(formDisplayName);

		if (formDisplayName.length() <= 28) {
			tableName = formDisplayName.trim().replaceAll("\\s+", "_");
		} else {
			String[] words = formDisplayName.split("\\s+");
			int numberOfWords = words.length;
			int numberOfCharsToInclude = (28 - (numberOfWords - 1)) / numberOfWords;

			for (String word : words) {
				word = word.trim();
				if (word.length() <= numberOfCharsToInclude)
					tableName = tableName + word + "_";
				else
					tableName = tableName + word.substring(0, numberOfCharsToInclude) + "_";
			}
			tableName = tableName.substring(0, tableName.length() - 1);

		}

		Form form = ((FormRepository) repository).findByTableNameAndApplication(tableName, app);
		if (form == null)
			return tableName;

		List<Form> forms = ((FormRepository) repository).findByTableNameLikeAndApplicationOrderByTableNameAsc(tableName + "___", app);
		
		List<Field> fields = fieldRepository.findByDatabasePropertyNameLikeAndApplicationAndMultiValueOrderByDatabasePropertyName(tableName + "___", app, true);
		
		char ch10 = '0', ch1 = '1';
		if (fields.size() != 0 || forms.size() != 0) {
			boolean add = false;
			String tblName = "";
			if(fields.size() != 0)tblName = fields.get(fields.size() - 1).getDatabasePropertyName();
			
			if(forms.size() != 0 && tblName.compareTo(forms.get(forms.size() - 1).getTableName()) < 0)
				tblName = forms.get(forms.size() - 1).getTableName();
			
			ch10 = tblName.charAt(tblName.length() - 2);
			ch1 = tblName.charAt(tblName.length() - 1);
			if (ch1 == '9' || ch1 == 'Z') {
				if (ch1 == '9')
					ch1 = 'A';
				else {
					ch1 = '0';
					add = true;
				}
			} else {
				ch1 = (char) (((int) ch1) + 1);
			}
			if (add) {
				if (ch10 == '9' || ch10 == 'Z') {
					if (ch10 == '9') {
						ch10 = 'A';
					} else
						ch10 = '0';
				} else {
					ch10 = (char) (((int) ch10) + 1);
				}
			}
		}

		tableName += "_" + ch10 + ch1;

		return tableName;
	}

	@Override
	protected List<Predicate> getConditionsPredicates(Root<Form> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {
		// TODO Auto-generated method stub

		List<Predicate> predicates = new ArrayList<>();
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			if (filterEntry.getKey().equals("applicationid")) {
				predicates.add(criteriaBuilder.equal(root.join("application").get("id"), filterEntry.getValue()));
			}
		}
		return predicates;

	}
}
