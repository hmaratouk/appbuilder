package com.technoliege.appbuilder.business;

import java.util.List;

import org.springframework.stereotype.Service;

import com.technoliege.appbuilder.model.Organization;
import com.technoliege.appbuilder.repository.OrganizationRepository;

@Service
public class OrganizationBusiness extends AbstractBusiness<Organization> {

	public OrganizationBusiness(OrganizationRepository repository) {
		// TODO Auto-generated constructor stub
		this.repository = repository;
	}
	
	public List<Organization> getOrganizations(){
		return ((OrganizationRepository)repository).findAll();
	}
	
	@Override
	public void saveDocument(Organization document) throws BusinessException {
		// TODO Auto-generated method stub
		super.saveDocument(document);
		document.setUniversalID( generateUniversalId(document, document.getId(), "09"));
		super.saveDocument(document);
	}
	
}
