package com.technoliege.appbuilder.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.technoliege.appbuilder.model.Environment;
import com.technoliege.appbuilder.model.User;
import com.technoliege.appbuilder.repository.UserRepository;

@Service
public class UserBusiness extends AbstractBusiness<User> {
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	OrganizationBusiness organizationBusiness;
	
	public UserBusiness(UserRepository repository) {
		this.repository = repository;
	}

	@Override
	public void saveDocument(User document) throws BusinessException {
		// TODO Auto-generated method stub

		// check if number of created users reached the maximum
		if (isMaximumNumberOfUsersReached(document) == true) {
			// TODO fix the message
			throw new BusinessException("cannot create a new user, maximum number of users have been already created");
		}

		// check if user name already exists
		if (isUserNameAlreadyUsed(document)) {
			// TODO fix the message
			throw new BusinessException("A user with the name already exists");
		}

		if (document.getId() == null) {
			int orgId = document.getOrganization().getId();
			String userName = document.getUsername();
			document.setOrgUserName(userName+"|"+orgId);
			document.setPassword(encoder.encode(document.getPassword()));
		}

		super.saveDocument(document);
		document.setUniversalID( generateUniversalId(document, document.getOrganization().getId(), "09"));
		super.saveDocument(document);
	}

	private boolean isUserNameAlreadyUsed(User document) throws com.technoliege.appbuilder.business.BusinessException {
		User user = ((UserRepository) repository).findByUsernameAndOrganization(document.getUsername(),
				document.getOrganization());

		if (user != null && !user.getId().equals(document.getId())) {
			return true;
		}
		return false;
	}

	// TODO - fix method name
	private boolean isMaximumNumberOfUsersReached(User document) throws BusinessException {
		int numberOfOrgUsers = ((UserRepository) repository).countByOrganization(document.getOrganization());
		
		if (numberOfOrgUsers == organizationBusiness.getDocument(document.getOrganization().getId()).getMaxNumberOfUsers()) {
			return true;
		}
		return false;
	}
	
	@Override
	protected List<Predicate> getConditionsPredicates(Root<User> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {
		// TODO Auto-generated method stub

		List<Predicate> predicates = new ArrayList<>();
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			if (filterEntry.getKey().equals("organizationid")) {
				predicates.add(criteriaBuilder.equal(root.join("organization").get("id"), filterEntry.getValue()));
			} 
		}
		return predicates;
	
	}
}
