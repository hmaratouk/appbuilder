package com.technoliege.appbuilder.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import java.util.Arrays;

import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;

import com.technoliege.appbuilder.model.Environment;
import com.technoliege.appbuilder.repository.EnvironmentRepository;

@Service
public class EnvironmentBusiness extends AbstractBusiness<Environment>{
	
	public EnvironmentBusiness(EnvironmentRepository repository) {
		this.repository = repository;
	}
	
	public List<Environment> getOrgEnvironment(Integer organizationId){
		return ((EnvironmentRepository) repository).findByOrganizationId(organizationId);
		
	}
	
	@Override
	protected List<Predicate> getConditionsPredicates(Root<Environment> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {
		// TODO Auto-generated method stub

		List<Predicate> predicates = new ArrayList<>();
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			if (filterEntry.getKey().equals("organizationid")) {
				predicates.add(criteriaBuilder.equal(root.join("organization").get("id"), filterEntry.getValue()));
			} 
		}
		return predicates;
	
	}
	@Override
	public void saveDocument(Environment document) throws BusinessException {
		// TODO Auto-generated method stub
		super.saveDocument(document);
		document.setUniversalID( generateUniversalId(document, document.getOrganization().getId(), "09"));
		super.saveDocument(document);
	}
}
