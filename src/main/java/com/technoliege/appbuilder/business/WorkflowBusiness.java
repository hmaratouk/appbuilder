package com.technoliege.appbuilder.business;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;
import com.technoliege.appbuilder.model.Form;
import com.technoliege.appbuilder.model.Field;
import com.technoliege.appbuilder.model.WorkFlow;
import com.technoliege.appbuilder.repository.WorkflowRepository;

@Service
public class WorkflowBusiness extends AbstractBusiness<WorkFlow>{

	public WorkflowBusiness(WorkflowRepository repository) {
		this.repository = repository;
	}
	
	public List<WorkFlow> getAppWorkFlow(Integer applicationId){
		return ((WorkflowRepository) repository).findByApplicationId(applicationId);
	}
	
	@Override
	protected List<Predicate> getConditionsPredicates(Root<WorkFlow> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {
		// TODO Auto-generated method stub

		List<Predicate> predicates = new ArrayList<>();
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			if (filterEntry.getKey().equals("applicationid")) {
				predicates.add(criteriaBuilder.equal(root.join("application").get("id"), filterEntry.getValue()));
			} 
		}
		return predicates;
	
	}
	@Override
	public void saveDocument(WorkFlow document) throws BusinessException {
		// TODO Auto-generated method stub
		super.saveDocument(document);
		document.setUniversalID( generateUniversalId(document, document.getOrganization().getId(), "09"));
		super.saveDocument(document);
	}
}