package com.technoliege.appbuilder.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;

import com.technoliege.appbuilder.model.Form;
import com.technoliege.appbuilder.model.ViewColumn;
import com.technoliege.appbuilder.repository.ViewColumnRepository;

@Service
public class ViewColumnBusiness extends AbstractBusiness<ViewColumn> {
	
	public ViewColumnBusiness(ViewColumnRepository repository) {
		this.repository = repository;
	}
	
	protected List<Predicate> getConditionsPredicates(Root<ViewColumn> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {
		// TODO Auto-generated method stub

		List<Predicate> predicates = new ArrayList<>();
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			if (filterEntry.getKey().equals("viewid")) {
				predicates.add(criteriaBuilder.equal(root.join("view").get("id"), filterEntry.getValue()));
			} 
		}
		return predicates;
	
	}
	
	@Override
	public void saveDocument(ViewColumn document) throws BusinessException {
		// TODO Auto-generated method stub
		super.saveDocument(document);
		document.setUniversalID( generateUniversalId(document, document.getOrganization().getId(), "09"));
		super.saveDocument(document);
	}
}
