package com.technoliege.appbuilder.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;

import com.technoliege.appbuilder.model.Form;
import com.technoliege.appbuilder.model.View;
import com.technoliege.appbuilder.repository.ViewRepository;

@Service
public class ViewBusiness extends AbstractBusiness<View> {

	public ViewBusiness(ViewRepository repository) {
		this.repository = repository;
	}
	
	public List<View> getAppViews(Integer applicationId){
		return ((ViewRepository) repository).findByApplicationId(applicationId);
	}
	

	protected List<Predicate> getConditionsPredicates(Root<View> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {
		// TODO Auto-generated method stub

		List<Predicate> predicates = new ArrayList<>();
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			if (filterEntry.getKey().equals("applicationid")) {
				predicates.add(criteriaBuilder.equal(root.join("application").get("id"), filterEntry.getValue()));
			} 
		}
		return predicates;
	
	}
	
	@Override
	public void saveDocument(View document) throws BusinessException {
		// TODO Auto-generated method stub
		super.saveDocument(document);
		document.setUniversalID( generateUniversalId(document, document.getOrganization().getId(), "09"));
		super.saveDocument(document);
	}
	
}
