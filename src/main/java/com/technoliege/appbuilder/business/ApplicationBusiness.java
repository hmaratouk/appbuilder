package com.technoliege.appbuilder.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;

import com.technoliege.appbuilder.model.Application;
import com.technoliege.appbuilder.model.Environment;
import com.technoliege.appbuilder.model.Form;
import com.technoliege.appbuilder.model.Organization;
import com.technoliege.appbuilder.model.View;
import com.technoliege.appbuilder.repository.ApplicationRepository;
import com.technoliege.appbuilder.repository.EnvironmentRepository;
import com.technoliege.appbuilder.repository.FormRepository;

@Service
public class ApplicationBusiness extends AbstractBusiness<Application> {

	public ApplicationBusiness(ApplicationRepository repository) {
		// TODO Auto-generated constructor stub
		this.repository = repository;
	}
	
	public List<Application> getEnvApplication(Integer environmentId){
		return ((ApplicationRepository) repository).findByEnvironmentId(environmentId);
		
	}
	
	@Override
	public void saveDocument(Application document) throws BusinessException {
		// TODO Auto-generated method stub
		//check if the display name is unique 
//		if(!checkDisplayName(document.getDisplayName(), document.getApplication() ) )
//			throw new BusinessException("");
		
		document.setApplicationSchema(generateSchemaName(document.getDisplayName(), document.getOrganization()));
		
		super.saveDocument(document);
		document.setUniversalID( generateUniversalId(document, document.getOrganization().getId(), "09"));
		super.saveDocument(document);
	}
	
	boolean checkDisplayName(String displayName, Organization org) {
		Application app = ((ApplicationRepository)repository).findByApplicationSchemaAndOrganization( displayName, org);
		if(app == null)
			return true;
		
		return false;
	}

	
	String generateSchemaName(String formDisplayName, Organization org) {

		String schemaName = "";
		
		 Pattern pt = Pattern.compile("[^a-zA-Z0-9_ ]");
	        Matcher match= pt.matcher(formDisplayName);
	        while(match.find())
	        {
	            String s= match.group();
	            formDisplayName=formDisplayName.replaceAll("\\"+s, "");
	        }
	       
		
		if (formDisplayName.length() <= 28) {
			schemaName = formDisplayName.trim().replaceAll("\\s+", "_");
		} else {
			String[] words = formDisplayName.split("\\s+");
			int numberOfWords = words.length;
			int numberOfCharsToInclude = (28 - (numberOfWords - 1)) / numberOfWords;

			for (String word : words) {
				word = word.trim();
				if (word.length() <= numberOfCharsToInclude)
					schemaName = schemaName + word + "_";
				else
					schemaName = schemaName + word.substring(0, numberOfCharsToInclude) + "_";
			}
			schemaName = schemaName.substring(0, schemaName.length() - 1);
			
		}
		
		Application app = ((ApplicationRepository)repository).findByApplicationSchemaAndOrganization(schemaName, org);
		if(app == null)
			return schemaName;
		List<Application> apps = ((ApplicationRepository)repository).findByApplicationSchemaLikeAndOrganizationOrderByApplicationSchema(schemaName+"___", org);
		
		char ch10 = '0', ch1 = '1';
		if (apps.size() != 0) {
			boolean add = false;
			String schemaAppName = apps.get(apps.size() - 1).getApplicationSchema();
			ch10 = schemaAppName.charAt(schemaAppName.length() - 2);
			ch1 = schemaAppName.charAt(schemaAppName.length() - 1);
			if (ch1 == '9' || ch1 == 'Z') {
				if (ch1 == '9')
					ch1 = 'A';
				else {
					ch1 = '0';
					add = true;
				}
			} else {
				ch1 = (char) (((int) ch1) + 1);
			}
			if (add) {
				if (ch10 == '9' || ch10 == 'Z') {
					if (ch10 == '9') {
						ch10 = 'A';
					} else
						ch10 = '0';
				} else {
					ch10 = (char) (((int) ch10) + 1);
				}
			}

		}
		schemaName += "_" + ch10 + ch1;
		 
		return schemaName;
	}
	
	protected List<Predicate> getConditionsPredicates(Root<Application> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {
		// TODO Auto-generated method stub

		List<Predicate> predicates = new ArrayList<>();
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			if (filterEntry.getKey().equals("environmentid")) {
				predicates.add(criteriaBuilder.equal(root.join("environment").get("id"), filterEntry.getValue()));
			} 
			if (filterEntry.getKey().equals("organizationid")) {
				predicates.add(criteriaBuilder.equal(root.join("organization").get("id"), filterEntry.getValue()));
			} 
		}
		return predicates;
	
	}

}
