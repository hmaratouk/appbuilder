package com.technoliege.appbuilder.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.technoliege.appbuilder.model.Application;
import com.technoliege.appbuilder.model.Field;
import com.technoliege.appbuilder.model.Form;
import com.technoliege.appbuilder.model.Organization;
import com.technoliege.appbuilder.model.ViewColumn;
import com.technoliege.appbuilder.repository.ApplicationRepository;
import com.technoliege.appbuilder.repository.FieldRepository;
import com.technoliege.appbuilder.repository.FormRepository;

@Service
public class FieldBusiness extends AbstractBusiness<Field> {

	public FieldBusiness(FieldRepository repository) {
		this.repository = repository;
	}
	
	@Autowired
	FormRepository formRepository;

	@Override
	public void saveDocument(Field document) throws BusinessException {
		// TODO Auto-generated method stub
		// check if the display name is unique
		// if(!checkDisplayName(document.getDisplayName(), document.getApplication() ) )
		// throw new BusinessException("");

		document.setDatabasePropertyName(generateDatabasePropertyName(document));
		

		super.saveDocument(document);
		document.setUniversalID( generateUniversalId(document, document.getOrganization().getId(), "09"));
		super.saveDocument(document);
	}

	boolean checkDisplayName(String displayName, Form form) {
		Field field = ((FieldRepository) repository).findByDatabasePropertyNameAndForm(displayName, form);
		if (field == null)
			return true;

		return false;
	}

	String generateDatabasePropertyName(Field doc) {

		String databasePropertyName = "";
		String formDisplayName = doc.getDisplayName();

		Pattern pt = Pattern.compile("[^a-zA-Z0-9_ ]");
		Matcher match = pt.matcher(formDisplayName);
		while (match.find()) {
			String s = match.group();
			formDisplayName = formDisplayName.replaceAll("\\" + s, "");
		}

		if (formDisplayName.length() <= 28) {
			databasePropertyName = formDisplayName.trim().replaceAll("\\s+", "_");
		} else {
			String[] words = formDisplayName.split("\\s+");
			int numberOfWords = words.length;
			int numberOfCharsToInclude = (27 - (numberOfWords - 1)) / numberOfWords;

			for (String word : words) {
				word = word.trim();
				if (word.length() <= numberOfCharsToInclude)
					databasePropertyName = databasePropertyName + word + "_";
				else
					databasePropertyName = databasePropertyName + word.substring(0, numberOfCharsToInclude) + "_";
			}
			databasePropertyName = databasePropertyName.substring(0, databasePropertyName.length() - 1);

		}
		List<Field> fields = null;
		List<Form> forms =null;
		if (!doc.getMultiValue()) {
			Form form = doc.getForm();

			Field field = ((FieldRepository) repository).findByDatabasePropertyNameAndFormAndMultiValue(databasePropertyName, form, false);
			if (field == null)
				return databasePropertyName;

			fields = ((FieldRepository) repository)
					.findByDatabasePropertyNameLikeAndFormAndMultiValue(databasePropertyName + "___", form, false);
			
		}
		else {
			Application app = doc.getApplication();
			Field field = ((FieldRepository) repository).findByDatabasePropertyNameAndApplicationAndMultiValue(databasePropertyName, app, true);
			Form form = formRepository.findByTableNameAndApplication(databasePropertyName, app);
			if (field == null && form == null)
				return databasePropertyName;

			forms = formRepository.findByTableNameLikeAndApplicationOrderByTableNameAsc(databasePropertyName+"___", app);
			 fields = ((FieldRepository) repository)
					.findByDatabasePropertyNameLikeAndApplicationAndMultiValueOrderByDatabasePropertyName(databasePropertyName + "___", app, true);	
		}

		char ch10 = '0', ch1 = '1';
		if (fields.size() != 0 || forms.size() != 0) {
			boolean add = false;
			String propName = "";
			if(fields.size() != 0)propName = fields.get(fields.size() - 1).getDatabasePropertyName();
			
			if(forms != null && forms.size() != 0 && propName.compareTo(forms.get(forms.size() - 1).getTableName()) < 0)
				propName = forms.get(forms.size() - 1).getTableName();
			
			ch10 = propName.charAt(propName.length() - 2);
			ch1 = propName.charAt(propName.length() - 1);
			if (ch1 == '9' || ch1 == 'Z') {
				if (ch1 == '9')
					ch1 = 'A';
				else {
					ch1 = '0';
					add = true;
				}
			} else {
				ch1 = (char) (((int) ch1) + 1);
			}
			if (add) {
				if (ch10 == '9' || ch10 == 'Z') {
					if (ch10 == '9') {
						ch10 = 'A';
					} else
						ch10 = '0';
				} else {
					ch10 = (char) (((int) ch10) + 1);
				}
			}

		}
		databasePropertyName += "_" + ch10 + ch1;

		System.out.println(databasePropertyName);
		return databasePropertyName;
	}

	protected List<Predicate> getConditionsPredicates(Root<Field> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {
		// TODO Auto-generated method stub

		List<Predicate> predicates = new ArrayList<>();
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			if (filterEntry.getKey().equals("formid")) {
				predicates.add(criteriaBuilder.equal(root.join("form").get("id"), filterEntry.getValue()));
			}
		}
		return predicates;

	}
}
