package com.technoliege.appbuilder.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import com.technoliege.appbuilder.model.Phase;
import com.technoliege.appbuilder.repository.PhaseRepository;

@Service
public class PhaseBusiness extends AbstractBusiness<Phase>{

	public PhaseBusiness (PhaseRepository repository) {
		this.repository = repository;
	}
	
	@Override
	protected List<Predicate> getConditionsPredicates(Root<Phase> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {
		// TODO Auto-generated method stub

		List<Predicate> predicates = new ArrayList<>();
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			if (filterEntry.getKey().equals("workflowid")) {
				predicates.add(criteriaBuilder.equal(root.join("workFlow").get("id"), filterEntry.getValue()));
			} 
		}
		return predicates;
	
	}
	@Override
	public void saveDocument(Phase document) throws BusinessException {
		// TODO Auto-generated method stub
		super.saveDocument(document);
		document.setUniversalID( generateUniversalId(document, document.getOrganization().getId(), "09"));
		super.saveDocument(document);
	}
}