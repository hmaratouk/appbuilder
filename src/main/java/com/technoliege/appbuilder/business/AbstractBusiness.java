package com.technoliege.appbuilder.business;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import com.technoliege.appbuilder.configuration.ApplicationConfiguration;
import com.technoliege.appbuilder.model.AbstractEntity;
import com.technoliege.appbuilder.model.Organization;
import com.technoliege.appbuilder.repository.MainRepository;

public class AbstractBusiness<T extends AbstractEntity> {
	
	public static int TypeIdSz = 2;
	public static int OrgIdSz = 4;
	public static int DocIdSz = 6;

	@Autowired
	protected ApplicationConfiguration applicationConfiguration;
	protected MainRepository<T, Integer> repository;

	public MainRepository<T, Integer> getRepository() {
		return repository;
	}

	public T getDocument(Integer id) {
		return getRepository().findById(id).orElse(null);
	}

	public void saveDocument(T document) throws BusinessException {
		getRepository().save(document);
	}

	public void deleteDocument(Integer id) throws BusinessException {
		getRepository().deleteById(id);
	}

	public final Map<String, Object> findDocuments(Map<String, String> filters) {

		int pageNumber = 0;
		int pageSize = 20;
		if (filters.containsKey("page_number") && filters.containsKey("page_size")) {
			pageNumber = Integer.parseInt(filters.remove("page_number"));
			pageSize = Integer.parseInt(filters.remove("page_size"));
		}
		final String sortField;
		if (filters.get("sort_field") == null || filters.get("sort_field").isEmpty()) {
			sortField = "lastEditedOn";
		} else {
			sortField = filters.remove("sort_field");
		}
		final String sortDirection;
		if (filters.get("sort_order") == null || filters.get("sort_order").isEmpty()) {
			sortDirection = "desc";
		} else {
			sortDirection = filters.remove("sort_order");
		}

		Specification<T> specification = new Specification<T>() {

			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

				query.distinct(true);
				Predicate predicate = criteriaBuilder
						.and(getConditionsPredicates(root, criteriaBuilder, filters).toArray(new Predicate[0]));
				Expression<String> sortExpression = getSortExpression(root, criteriaBuilder, sortField);
				if (sortDirection.equals("asc")) {
					query.orderBy(criteriaBuilder.asc(sortExpression));
				} else {
					query.orderBy(criteriaBuilder.desc(sortExpression));
				}
				return predicate;
			}
		};

		Map<String, Object> result = new LinkedHashMap<>();
		List<T> documents;
		if (pageSize == -1) {
			documents = getRepository().findAll(specification);
		} else {
			Page<T> page = getRepository().findAll(specification, PageRequest.of(pageNumber, pageSize));
			documents = new ArrayList<>();
			page.forEach(documents::add);
			result.put("totalElements", page.getTotalElements());
		}
		result.put("rows", documents);
		return result;
	}

	protected List<Predicate> getConditionsPredicates(Root<T> root, CriteriaBuilder criteriaBuilder,
			Map<String, String> filters) {

		List<Predicate> conditions = new ArrayList<>();
		for (Entry<String, String> filterEntry : filters.entrySet()) {
			if (filterEntry.getValue() == null || filterEntry.getValue().isEmpty()) {
				continue;
			}
			conditions.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(filterEntry.getKey())),
					"%" + filterEntry.getValue().toLowerCase() + "%"));
		}
		return conditions;
	}

	protected Expression<String> getSortExpression(Root<T> root, CriteriaBuilder criteriaBuilder, String sortField) {
		return root.get(sortField);
	}
	
	public String generateUniversalId(T document, int org ,String typeId) {
		
		String orgId = ""+org, docId = document.getId()+"";
		
		if(orgId.length() > OrgIdSz)
			orgId = orgId.substring(orgId.length()-OrgIdSz);
		else {
			int sz = orgId.length();
			for(int i=0; i< OrgIdSz-sz;i++)
				orgId = "0"+orgId;
		}
		if(docId.length() > DocIdSz)
			docId = docId.substring(docId.length()-DocIdSz);
		else {
			int sz = docId.length();
			for(int i=0; i< DocIdSz-sz;i++)
				docId = "0"+docId;
		}
		return   orgId + typeId+docId;
		
	}

}
