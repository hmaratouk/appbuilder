package com.technoliege.appbuilder.repository;

import java.util.List;

import com.technoliege.appbuilder.model.Environment;
import com.technoliege.appbuilder.model.Organization;

public interface EnvironmentRepository extends MainRepository<Environment, Integer>{

	public List<Environment> findByOrganizationId(Integer organizationId);
}
