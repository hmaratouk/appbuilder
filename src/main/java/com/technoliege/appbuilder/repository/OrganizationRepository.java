package com.technoliege.appbuilder.repository;

import java.util.List;

import com.technoliege.appbuilder.model.Organization;

public interface OrganizationRepository extends MainRepository<Organization, Integer> {

	public List<Organization> findAll();
}
