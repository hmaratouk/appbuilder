package com.technoliege.appbuilder.repository;


import java.util.List;



import com.technoliege.appbuilder.model.Application;
import com.technoliege.appbuilder.model.Form;

public interface FormRepository extends MainRepository<Form, Integer>{
	public Form findByDisplayNameAndApplication(String displayName, Application application);
	public Form findByTableNameAndApplication(String name, Application application);
	
	public List<Form> findByApplicationId(Integer applicationId);
	public List<Form> findByTableNameLikeAndApplicationOrderByTableNameAsc(String string, Application app);
}
