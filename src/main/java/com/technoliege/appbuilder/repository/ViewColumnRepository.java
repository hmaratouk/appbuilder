package com.technoliege.appbuilder.repository;

import com.technoliege.appbuilder.model.ViewColumn;

public interface ViewColumnRepository extends MainRepository<ViewColumn, Integer>{

}
