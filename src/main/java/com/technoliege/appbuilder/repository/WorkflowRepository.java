package com.technoliege.appbuilder.repository;

import java.util.List;

import com.technoliege.appbuilder.model.WorkFlow;

public interface WorkflowRepository  extends MainRepository<WorkFlow, Integer>{

	public List<WorkFlow> findByApplicationId(Integer applicationId);
}
