package com.technoliege.appbuilder.repository;

import com.technoliege.appbuilder.model.Organization;
import com.technoliege.appbuilder.model.User;

public interface UserRepository extends MainRepository<User, Integer> {

	public int countByOrganization(Organization organization);
	public User findByUsername(String username);
	public User findByUsernameAndOrganization(String username, Organization organization);
}
