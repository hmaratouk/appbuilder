package com.technoliege.appbuilder.repository;

import com.technoliege.appbuilder.model.Phase;

public interface PhaseRepository extends MainRepository<Phase, Integer> {

}
