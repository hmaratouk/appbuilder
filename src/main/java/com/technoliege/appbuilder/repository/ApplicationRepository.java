package com.technoliege.appbuilder.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.technoliege.appbuilder.model.Application;
import com.technoliege.appbuilder.model.Form;
import com.technoliege.appbuilder.model.Organization;

@Repository
public interface ApplicationRepository extends MainRepository<Application, Integer> {

	
	public Application findByApplicationSchemaAndOrganization(String name, Organization organization);
	
	public List<Application> findByApplicationSchemaLikeAndOrganizationOrderByApplicationSchema(String name, Organization organization);
	
	public List<Application> findByEnvironmentId(Integer environmentId);
}
