package com.technoliege.appbuilder.repository;

import java.util.List;

import com.technoliege.appbuilder.model.View;

public interface ViewRepository extends MainRepository<View, Integer> {

	public List<View> findByApplicationId(Integer applicationId);
}
