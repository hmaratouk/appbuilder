package com.technoliege.appbuilder.repository;

import java.util.List;

import com.technoliege.appbuilder.model.Application;
import com.technoliege.appbuilder.model.Field;
import com.technoliege.appbuilder.model.Form;

public interface FieldRepository extends MainRepository<Field, Integer>{

	
	public Field findByDatabasePropertyNameAndForm(String name, Form form);
	
	public Field findByDatabasePropertyNameAndFormAndMultiValue(String name, Form form, Boolean multiValue);
	public List<Field> findByDatabasePropertyNameLikeAndFormAndMultiValue(String name, Form form, Boolean multiValue);
	
	
	
	public Field findByDatabasePropertyNameAndApplicationAndMultiValue(String name, Application app, Boolean multiValue);
	public List<Field> findByDatabasePropertyNameLikeAndApplicationAndMultiValueOrderByDatabasePropertyName(String name, Application app , Boolean multiValue);
}
