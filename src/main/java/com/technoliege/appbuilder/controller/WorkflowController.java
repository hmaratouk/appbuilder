package com.technoliege.appbuilder.controller;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.technoliege.appbuilder.business.BusinessException;
import com.technoliege.appbuilder.business.ViewBusiness;
import com.technoliege.appbuilder.business.WorkflowBusiness;
import com.technoliege.appbuilder.model.JsonSerializationViews;
import com.technoliege.appbuilder.model.View;
import com.technoliege.appbuilder.model.WorkFlow;


@CrossOrigin
@RestController
public class WorkflowController extends AbstractController<WorkFlow> {

	public WorkflowController(WorkflowBusiness business) {
		this.business = business;
	}
	
	@Override
	@GetMapping("/workflows/{workflow_id}")
	@JsonView({JsonSerializationViews.WorkflowForm.class})
	public WorkFlow getDocument(@PathVariable("workflow_id") Integer documentId) {
		// TODO Auto-generated method stub
		return super.getDocument(documentId);
	}
	
	@Override
	@GetMapping("/workflows")
	@JsonView({JsonSerializationViews.WorkflowView.class})
	public Map<String, Object> getDocuments(@RequestParam Map<String, String> filters) {
		// TODO Auto-generated method stub
		return super.getDocuments(filters);
	}
	
	@Override
	@PostMapping("/workflows")
	@JsonView({JsonSerializationViews.WorkflowForm.class})
	public WorkFlow addDocument(@RequestBody WorkFlow document) throws BusinessException {
		// TODO Auto-generated method stub
		return super.addDocument(document);
	}
	
	@Override
	@PutMapping("/workflows/{workflow_id}")
	public void updateDocument(@PathVariable("workflow_id") Integer documentId, @RequestBody WorkFlow document) throws BusinessException {
		// TODO Auto-generated method stub
		super.updateDocument(documentId, document);
	}
	
	@Override
	@DeleteMapping("/workflows/{workflow_id}")
	public void deleteDocument(@PathVariable("workflow_id") Integer documentId) throws BusinessException {
		// TODO Auto-generated method stub
		super.deleteDocument(documentId);
	}
	
	@GetMapping("/app-workflows")
	@JsonView({JsonSerializationViews.metadata.class})
	public List<WorkFlow> getAppWorkFlow(@RequestParam ("applicationid") Integer applicationId) {
		// TODO Auto-generated method stub
		return ((WorkflowBusiness)business).getAppWorkFlow(applicationId);
	}
}