package com.technoliege.appbuilder.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.RequestParam;

import com.technoliege.appbuilder.business.AbstractBusiness;
import com.technoliege.appbuilder.business.BusinessException;
import com.technoliege.appbuilder.model.AbstractEntity;

public class AbstractController<T extends AbstractEntity> {

	protected AbstractBusiness<T> business;

	public Map<String, Object> getDocuments(@RequestParam Map<String, String> filters) {
		return business.findDocuments(filters);
	}

	public T getDocument(Integer documentId) {
		return business.getDocument(documentId);
	}

	public T addDocument(T document) throws BusinessException {
		business.saveDocument(document);
		return document;
	}

	public void updateDocument(Integer documentId, T document) throws BusinessException {
		document.setId(documentId);
		business.saveDocument(document);
	}

	public void deleteDocument(Integer documentId) throws BusinessException {
		business.deleteDocument(documentId);
	}
}
