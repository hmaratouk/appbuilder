package com.technoliege.appbuilder.controller;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.technoliege.appbuilder.business.ApplicationBusiness;
import com.technoliege.appbuilder.business.BusinessException;
import com.technoliege.appbuilder.business.EnvironmentBusiness;
import com.technoliege.appbuilder.model.Application;
import com.technoliege.appbuilder.model.Environment;
import com.technoliege.appbuilder.model.JsonSerializationViews;

@CrossOrigin
@RestController
public class ApplicationController extends AbstractController<Application> {

	public ApplicationController(ApplicationBusiness business) {
		this.business = business;
	}

	@Override
	@GetMapping("/applications/{application_id}")
	@JsonView({JsonSerializationViews.ApplicationForm.class})
	public Application getDocument(@PathVariable("application_id") Integer documentId) {
		// TODO Auto-generated method stub
		return super.getDocument(documentId);
	}

	@Override
	@GetMapping("/applications")
	@JsonView({JsonSerializationViews.ApplicationView.class})
	public Map<String, Object> getDocuments(@RequestParam Map<String, String> filters) {
		// TODO Auto-generated method stub
		return super.getDocuments(filters);
	}

	@Override
	@PostMapping("/applications")
	@JsonView({JsonSerializationViews.ApplicationForm.class})
	public Application addDocument(@RequestBody Application document) throws BusinessException {
		// TODO Auto-generated method stub
		return super.addDocument(document);
	}

	@Override
	@PutMapping("/applications/{application_id}")
	public void updateDocument(@PathVariable("application_id") Integer documentId, @RequestBody Application document)
			throws BusinessException {
		// TODO Auto-generated method stub
		super.updateDocument(documentId, document);
	}

	@Override
	@DeleteMapping("/applications/{application_id}")
	public void deleteDocument(@PathVariable("application_id") Integer documentId) throws BusinessException {
		// TODO Auto-generated method stub
		super.deleteDocument(documentId);
	}
	
	@GetMapping("/env-applications/{environment_id}")
	@JsonView({JsonSerializationViews.AbstractView.class})
	public List<Application> getEnvApplication(@PathVariable("environment_id") Integer environmentId) {
		// TODO Auto-generated method stub
		return ((ApplicationBusiness)business).getEnvApplication(environmentId);
	}

}
