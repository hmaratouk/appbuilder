package com.technoliege.appbuilder.controller;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonValueInstantiator;
import com.technoliege.appbuilder.business.BusinessException;
import com.technoliege.appbuilder.business.OrganizationBusiness;
import com.technoliege.appbuilder.model.JsonSerializationViews;
import com.technoliege.appbuilder.model.Organization;

@CrossOrigin
@RestController
public class OrganizationController extends AbstractController<Organization> {

	public OrganizationController(OrganizationBusiness business) {
		this.business = business;
	}
	
	@Override
	@GetMapping("/organizations/{organization_id}")
	@JsonView({JsonSerializationViews.OrganizationForm.class})
	public Organization getDocument(@PathVariable("organization_id") Integer documentId) {
		// TODO Auto-generated method stub
		return super.getDocument(documentId);
	}
	
	@Override
	@GetMapping("/organizations")
	@JsonView({JsonSerializationViews.OrganizationView.class})
	public Map<String, Object> getDocuments(@RequestParam Map<String, String> filters) {
		// TODO Auto-generated method stub
		return super.getDocuments(filters);
	}
	
	@Override
	@PostMapping("/organizations")
	@JsonView({JsonSerializationViews.OrganizationForm.class})
	public Organization addDocument(@RequestBody Organization document) throws BusinessException {
		// TODO Auto-generated method stub
		return super.addDocument(document);
	}
	
	@Override
	@PutMapping("/organizations/{organization_id}")
	public void updateDocument(@PathVariable("organization_id") Integer documentId, @RequestBody Organization document) throws BusinessException {
		// TODO Auto-generated method stub
		super.updateDocument(documentId, document);
	}
	
	@Override
	@DeleteMapping("/organizations/{organization_id}")
	public void deleteDocument(@PathVariable("organization_id") Integer documentId) throws BusinessException {
		// TODO Auto-generated method stub
		super.deleteDocument(documentId);
	}
	
	@GetMapping("/login-orgs")
	@JsonView({JsonSerializationViews.loginForm.class})
	public List<Organization> getLoginOrganization() {
		// TODO Auto-generated method stub
		return ((OrganizationBusiness)business).getOrganizations();
	}
	
	//Runtime requests
	
	@GetMapping("/metadata/organizations")
	@JsonView({JsonSerializationViews.metadata.class})
	public List<Organization> getOrganizationsMetadata() {
		// TODO Auto-generated method stub
		return ((OrganizationBusiness)business).getOrganizations();
	}
	
}
