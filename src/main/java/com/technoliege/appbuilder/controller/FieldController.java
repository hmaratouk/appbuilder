package com.technoliege.appbuilder.controller;

import java.util.Map;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.technoliege.appbuilder.business.BusinessException;
import com.technoliege.appbuilder.business.FieldBusiness;
import com.technoliege.appbuilder.model.Field;
import com.technoliege.appbuilder.model.JsonSerializationViews;

@CrossOrigin
@RestController
public class FieldController extends AbstractController<Field>{

	public FieldController(FieldBusiness busienss) {
		this.business = busienss;
	}
	
	@Override
	@GetMapping("/fields/{field_id}")
	@JsonView({JsonSerializationViews.FieldForm.class})
	public Field getDocument(@PathVariable("field_id")Integer documentId) {
		// TODO Auto-generated method stub
		return super.getDocument(documentId);
	}
	
	@Override
	@GetMapping("/fields")
	@JsonView({JsonSerializationViews.FieldView.class})
	public Map<String, Object> getDocuments(@RequestParam Map<String, String> filters) {
		// TODO Auto-generated method stub
		return super.getDocuments(filters);
	}
	
	@Override
	@PostMapping("/fields")
	@JsonView({JsonSerializationViews.FieldForm.class})
	public Field addDocument(@RequestBody Field document) throws BusinessException {
		// TODO Auto-generated method stub
		return super.addDocument(document);
	}
	
	@Override
	@PutMapping("/fields/{field_id}")
	public void updateDocument(@PathVariable("field_id") Integer documentId, @RequestBody Field document) throws BusinessException {
		// TODO Auto-generated method stub
		super.updateDocument(documentId, document);
	}
	
	@Override
	@DeleteMapping("/fields/{field_id}")
	public void deleteDocument(@PathVariable("field_id") Integer documentId) throws BusinessException {
		// TODO Auto-generated method stub
		super.deleteDocument(documentId);
	}
}
