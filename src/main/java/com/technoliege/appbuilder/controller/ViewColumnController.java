package com.technoliege.appbuilder.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.technoliege.appbuilder.business.BusinessException;
import com.technoliege.appbuilder.business.ViewColumnBusiness;
import com.technoliege.appbuilder.model.JsonSerializationViews;
import com.technoliege.appbuilder.model.ViewColumn;

@CrossOrigin
@RestController
public class ViewColumnController extends AbstractController<ViewColumn> {

	public ViewColumnController(ViewColumnBusiness business) {
		this.business = business;
	}
	
	@Override
	@GetMapping("/viewcolumns/{viewcolumn_id}")
	@JsonView({JsonSerializationViews.ViewColumnForm.class})
	public ViewColumn getDocument(@PathVariable("viewcolumn_id") Integer documentId) {
		// TODO Auto-generated method stub
		return super.getDocument(documentId);
	}
	
	@Override
	@GetMapping("/viewcolumns")
	@JsonView({JsonSerializationViews.ViewColumnView.class})
	public Map<String, Object> getDocuments(@RequestParam Map<String, String> filters) {
		// TODO Auto-generated method stub
		return super.getDocuments(filters);
	}
	
	@Override
	@PostMapping("/viewcolumns")
	@JsonView({JsonSerializationViews.ViewColumnForm.class})
	public ViewColumn addDocument(@RequestBody ViewColumn document) throws BusinessException {
		// TODO Auto-generated method stub
		return super.addDocument(document);
	}
	
	@Override
	@PutMapping("/viewcolumns/{viewcolumn_id}")
	public void updateDocument(@PathVariable("viewcolumn_id") Integer documentId, @RequestBody ViewColumn document) throws BusinessException {
		// TODO Auto-generated method stub
		super.updateDocument(documentId, document);
	}
	
	@Override
	@DeleteMapping("/viewcolumns/{viewcolumn_id}")
	public void deleteDocument(@PathVariable("viewcolumn_id") Integer documentId) throws BusinessException {
		// TODO Auto-generated method stub
		super.deleteDocument(documentId);
	}
	

}
