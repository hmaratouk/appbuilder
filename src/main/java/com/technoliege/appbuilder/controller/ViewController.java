package com.technoliege.appbuilder.controller;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.technoliege.appbuilder.business.BusinessException;
import com.technoliege.appbuilder.business.FormBusiness;
import com.technoliege.appbuilder.business.ViewBusiness;
import com.technoliege.appbuilder.model.Form;
import com.technoliege.appbuilder.model.JsonSerializationViews;
import com.technoliege.appbuilder.model.View;

@CrossOrigin
@RestController
public class ViewController extends AbstractController<View>{
	
	public ViewController(ViewBusiness business) {
		this.business = business;
	}
	
	@Override
	@GetMapping("/views/{view_id}")
	@JsonView({JsonSerializationViews.ViewForm.class})
	public View getDocument(@PathVariable("view_id")Integer documentId) {
		// TODO Auto-generated method stub
		return super.getDocument(documentId);
	}
	
	@Override
	@GetMapping("/views")
	@JsonView({JsonSerializationViews.ViewView.class})
	public Map<String, Object> getDocuments(@RequestParam Map<String, String> filters) {
		// TODO Auto-generated method stub
		return super.getDocuments(filters);
	}
	
	@Override
	@PostMapping("/views")
	@JsonView({JsonSerializationViews.ViewForm.class})
	public View addDocument(@RequestBody View document) throws BusinessException {
		// TODO Auto-generated method stub
		return super.addDocument(document);
	}
	
	@Override
	@PutMapping("/views/{view_id}")
	public void updateDocument(@PathVariable("view_id") Integer documentId, @RequestBody View document) throws BusinessException {
		// TODO Auto-generated method stub
		super.updateDocument(documentId, document);
	}
	
	@Override
	@DeleteMapping("/views/{view_id}")
	public void deleteDocument(@PathVariable("view_id") Integer documentId) throws BusinessException {
		// TODO Auto-generated method stub
		super.deleteDocument(documentId);
	}
	
	@GetMapping("/app-views/{application_id}")
	@JsonView({JsonSerializationViews.AbstractView.class})
	public List<View> getAppViews(@PathVariable("application_id") Integer applicationId) {
		// TODO Auto-generated method stub
		return ((ViewBusiness)business).getAppViews(applicationId);
	}
	
}
