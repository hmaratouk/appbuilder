package com.technoliege.appbuilder.controller;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.jooq.AlterTableFinalStep;
import org.jooq.AlterTableStep;
import org.jooq.CreateTableAsStep;
import org.jooq.CreateTableColumnStep;
import org.jooq.DSLContext;
import org.jooq.DataType;
import org.jooq.Record;
import org.jooq.SQL;
import org.jooq.SQLDialect;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.UpdateSetFirstStep;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.util.mysql.MySQLDataType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.technoliege.appbuilder.business.ApplicationBusiness;
import com.technoliege.appbuilder.model.Application;
import com.technoliege.appbuilder.model.AssignmentType;
import com.technoliege.appbuilder.model.EditabilityStyle;
import com.technoliege.appbuilder.model.Field;
import com.technoliege.appbuilder.model.FieldType;
import com.technoliege.appbuilder.model.Form;
import com.technoliege.appbuilder.model.OrderType;
import com.technoliege.appbuilder.model.Theme;
import com.technoliege.appbuilder.model.ThemeMarker;


@CrossOrigin
@RestController
public class GeneralController {
	
	Map<FieldType,DataType<?>> systemToDatabsaeType = new HashMap<FieldType, DataType<?>>();
	
	String userName = "root";
    String password = "root";
    String url = "jdbc:mysql://localhost:3306";
	
	@Autowired
	ApplicationBusiness applicationBusiness;
	
	@GetMapping("/theme-marker")
	public ResponseEntity<ThemeMarker[]> getThemeMarker() {
		return ResponseEntity.ok(ThemeMarker.values());
	}
	
	@GetMapping("/assignment-types")
	public ResponseEntity<AssignmentType[]> getAssignmentType() {
		return ResponseEntity.ok(AssignmentType.values());
	}

	@GetMapping("/themes")
	public ResponseEntity<Theme[]> themes() {
		return ResponseEntity.ok(Theme.values());
	}
	
	@GetMapping("/order-types")
	public ResponseEntity<OrderType[]> orderTypes() {
		return ResponseEntity.ok(OrderType.values());
	}
	
	@GetMapping("/field-types")
	public ResponseEntity<FieldType[]> fieldTypes() {
		return ResponseEntity.ok(FieldType.values());
	}
	
	@GetMapping("/editability-style")
	public ResponseEntity<EditabilityStyle[]> editabilityStyle() {
		return ResponseEntity.ok(EditabilityStyle.values());
	}
	
	@GetMapping("/deploy")
	public void deployDataBase(Map<String,String> filters) throws SQLException {

		mapTypes();
		 try (Connection conn = DriverManager.getConnection(url, userName, password)) {
	        	DSLContext create = DSL.using(conn, SQLDialect.MYSQL);
	        	//Get all organization's application (check specifying organization) 
	        	filters.put("page_size", "-1");
	    		Map<String, Object> applicationsMap = applicationBusiness.findDocuments(filters);
	    		List<Application> applications = (List<Application>) applicationsMap.get("rows");
	    		
	    		List<Schema> schemas = null;
	    		
	        	for(Application application : applications) {
	        		schemas = create.meta().getSchemas();
	        		//Create schema for the application if not already created
	    			String applicationSchemaName = application.getApplicationSchema();
	    			create.createSchemaIfNotExists(applicationSchemaName).execute();
	    			//Switch to the created schema to create forms tables
	    			create.setSchema(applicationSchemaName).execute();
	    			
	    			Schema applicationSchema = create.meta().getSchemas(applicationSchemaName.toLowerCase()).get(0);
	    			//get all application forms tables (will get forms tables if already existed, else, will get an empty list)
    				List<Table<?>> appliactionTables = applicationSchema.getTables();
    				
	    			for(Form form : application.getForms()) {
	    				//Create table for the form if not already created
	    				String formTableName = form.getTableName();   				
	    				CreateTableAsStep<Record> table = create.createTableIfNotExists(formTableName);
	    				CreateTableColumnStep column = null;

	    				//Create id column for the form table - to store form documents ids in the runtime environment
	    				column = table.column("id", SQLDataType.BIGINT.identity(true));
	    				column.constraint(DSL.constraint("pk_name").primaryKey("id"));
	    				column.execute();
	    				
	    				Table formTable = applicationSchema.getTable(formTableName.toLowerCase());
	    				org.jooq.Field[] formTableColumns = formTable.fields();
	    				List<org.jooq.Field> formTableColumnsList = new LinkedList<org.jooq.Field>(Arrays.asList(formTableColumns));
	    				//the formTableColumnList will be used to hold the columns that are no longer needed in the tables -no longer a part from the meta data- id is removed from the list not to be considered as a column to be removed
	    				org.jooq.Field idColumn = formTable.field("id");
	    				formTableColumnsList.remove(idColumn);

	    				AlterTableStep alterTable = create.alterTable(formTableName);
	    				AlterTableFinalStep addColumn = null;
	    				AlterTableFinalStep dropColumn = null;
	    				String fieldDataBasePropertyName = null;
	    				//add all non-existing columns
	    				for(Field field : form.getFields()) {
	    					fieldDataBasePropertyName = field.getDatabasePropertyName();
	    					
	    					//Create a join table to persist multi value field values, and a column in the form table for single value field
	    					if(field.getMultiValue() == false) {
	    						//get the field column, if not existed, create it , then drop it both ways from the columns list -as it is actually a part of the meta data-
	    						org.jooq.Field fieldColumn = formTable.field(fieldDataBasePropertyName,systemToDatabsaeType.get(field.getFieldType()));     //fix when a column is not deleted but its data type is changed
	    						if(fieldColumn == null) {
	    							addColumn = alterTable.addColumn(fieldDataBasePropertyName, systemToDatabsaeType.get(field.getFieldType()));
	    							addColumn.execute();
	    						}
	    						formTableColumnsList.remove(fieldColumn);
	    					} else {
	    						create.createTableIfNotExists(fieldDataBasePropertyName).column(field.getDocumentColumnName(), SQLDataType.NUMERIC(2)).column(field.getMultiValueColumnName(), SQLDataType.NUMERIC(2)).execute();
	    						appliactionTables.remove(applicationSchema.getTable(fieldDataBasePropertyName.toLowerCase()));
	    					}
	    				}
	    				
//	    				if(addColumn != null) 
//	    					addColumn.execute();
	    				
	    				for(org.jooq.Field tableColumn : formTableColumnsList) {
	    					dropColumn = alterTable.dropColumn(tableColumn);
	    					dropColumn.execute();
	    				}
	    				
//	    				if(dropColumn != null) {
//	    					dropColumn.execute();
//	    				}
//	    				
	    				appliactionTables.remove(formTable);
	    				
	    			}
	    			
	    			//loop on applicationTables and drop all remaining tables
	    			
	    			for(Table table : appliactionTables) {
	    				create.dropTable(table).execute();
	    			}
	    			
	    			schemas.remove(applicationSchema);
	    		}
	        	
//	        	for(Schema schema : schemas) {
//	        		create.dropSchema(schema).execute();
//	        	}
	        } 
	}
	
	void mapTypes() {
		systemToDatabsaeType.put(FieldType.CURRENCY, SQLDataType.DOUBLE.length(50));
		systemToDatabsaeType.put(FieldType.DATE_TIME, SQLDataType.DATE);
		systemToDatabsaeType.put(FieldType.EMAIL,SQLDataType.VARCHAR(500));
		systemToDatabsaeType.put(FieldType.NUMBER, SQLDataType.NUMERIC(50));
		systemToDatabsaeType.put(FieldType.PASSWORD, SQLDataType.VARCHAR(100));
		systemToDatabsaeType.put(FieldType.RADIO_BUTTON, SQLDataType.INTEGER.length(2));
		systemToDatabsaeType.put(FieldType.SINGLE_VALUE_ATTACHMENT, SQLDataType.VARCHAR(500));
		systemToDatabsaeType.put(FieldType.SINGLE_VALUE_CHECKBOX, SQLDataType.INTEGER.length(2));
		systemToDatabsaeType.put(FieldType.SINGLE_VALUE_DROPDOWN, SQLDataType.INTEGER.length(2));
		systemToDatabsaeType.put(FieldType.SINGLE_VALUE_LINK, SQLDataType.INTEGER.length(2));
		systemToDatabsaeType.put(FieldType.SINGLE_VALUE_LIST, SQLDataType.INTEGER.length(2));
		systemToDatabsaeType.put(FieldType.SINGLE_VALUE_USER, SQLDataType.INTEGER.length(2));
		systemToDatabsaeType.put(FieldType.TEXT, SQLDataType.VARCHAR(400));
		systemToDatabsaeType.put(FieldType.TEXT_AREA, SQLDataType.VARCHAR(2000));
	}

}
