package com.technoliege.appbuilder.controller;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.technoliege.appbuilder.business.BusinessException;
import com.technoliege.appbuilder.business.EnvironmentBusiness;

import com.technoliege.appbuilder.model.Environment;
import com.technoliege.appbuilder.model.JsonSerializationViews;
import com.technoliege.appbuilder.model.JsonSerializationViews.AbstractView;



@CrossOrigin
@RestController
public class EnvironmentController extends AbstractController<Environment> {

	public EnvironmentController(EnvironmentBusiness business) {
		this.business = business;
	}
	
	@Override
	@GetMapping("/environments/{environment_id}")
	@JsonView({JsonSerializationViews.EnvironmentForm.class})
	public Environment getDocument(@PathVariable("environment_id") Integer documentId) {
		// TODO Auto-generated method stub
		return super.getDocument(documentId);
	}
	
	@Override
	@GetMapping("/environments")
	@JsonView({JsonSerializationViews.EnvironmentView.class})
	public Map<String, Object> getDocuments(@RequestParam Map<String, String> filters) {
		// TODO Auto-generated method stub
		return super.getDocuments(filters);
	}
	
	@Override
	@PostMapping("/environments")
	@JsonView({JsonSerializationViews.EnvironmentForm.class})
	public Environment addDocument(@RequestBody Environment document) throws BusinessException {
		// TODO Auto-generated method stub
		return super.addDocument(document);
	}
	
	@Override
	@PutMapping("/environments/{environment_id}")
	public void updateDocument(@PathVariable("environment_id") Integer documentId, @RequestBody Environment document) throws BusinessException {
		// TODO Auto-generated method stub
		super.updateDocument(documentId, document);
	}
	
	@Override
	@DeleteMapping("/environments/{environment_id}")
	public void deleteDocument(@PathVariable("environment_id") Integer documentId) throws BusinessException {
		// TODO Auto-generated method stub
		super.deleteDocument(documentId);
	}
	
	@GetMapping("/org-environments/{organization_id}")
	@JsonView({JsonSerializationViews.AbstractView.class})
	public List<Environment> getOrgEnvironment(@PathVariable("organization_id") Integer organizationId) {
		// TODO Auto-generated method stub
		return ((EnvironmentBusiness)business).getOrgEnvironment(organizationId);
	}
	
}