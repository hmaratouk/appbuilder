package com.technoliege.appbuilder.controller;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.technoliege.appbuilder.business.ApplicationBusiness;
import com.technoliege.appbuilder.business.BusinessException;
import com.technoliege.appbuilder.business.FormBusiness;
import com.technoliege.appbuilder.model.Application;
import com.technoliege.appbuilder.model.Form;
import com.technoliege.appbuilder.model.JsonSerializationViews;



@CrossOrigin
@RestController
public class FormController extends AbstractController<Form>{

	public FormController (FormBusiness business) {
		this.business = business;
	}
	
	@Override
	@GetMapping("/forms/{form_id}")
	@JsonView({JsonSerializationViews.FormForm.class})
	public Form getDocument(@PathVariable("form_id") Integer documentId) {
		// TODO Auto-generated method stub
		return super.getDocument(documentId);
	}
	
	@Override
	@GetMapping("/forms")
	@JsonView({JsonSerializationViews.FormView.class})
	public Map<String, Object> getDocuments(@RequestParam Map<String, String> filters) {
		// TODO Auto-generated method stub
		return super.getDocuments(filters);
	}
	
	@Override
	@PostMapping("/forms")
	@JsonView({JsonSerializationViews.FormForm.class})
	public Form addDocument(@RequestBody Form document) throws BusinessException {
		// TODO Auto-generated method stub
		return super.addDocument(document);
	}
	
	@Override
	@PutMapping("/forms/{form_id}")
	public void updateDocument(@PathVariable("form_id") Integer documentId, @RequestBody Form document) throws BusinessException {
		// TODO Auto-generated method stub
		super.updateDocument(documentId, document);
	}
	
	@Override
	@DeleteMapping("/forms/{form_id}")
	public void deleteDocument(@PathVariable("form_id") Integer documentId) throws BusinessException {
		// TODO Auto-generated method stub
		super.deleteDocument(documentId);
	}
	
	@GetMapping("/app-forms/{application_id}")
	@JsonView({JsonSerializationViews.AbstractView.class})
	public List<Form> getAppForms(@PathVariable("application_id") Integer applicationId) {
		// TODO Auto-generated method stub
		return ((FormBusiness)business).getAppForms(applicationId);
	}
}
