package com.technoliege.appbuilder.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.technoliege.appbuilder.business.BusinessException;
import com.technoliege.appbuilder.business.PhaseBusiness;
import com.technoliege.appbuilder.model.JsonSerializationViews;
import com.technoliege.appbuilder.model.Phase;

@CrossOrigin
@RestController
public class PhaseController extends AbstractController<Phase> {

	public PhaseController(PhaseBusiness business) {
		this.business = business;
	}
	
	@Override
	@GetMapping("/phases/{phase_id}")
	@JsonView({JsonSerializationViews.PhaseForm.class})
	public Phase getDocument(@PathVariable("phase_id") Integer documentId) {
		// TODO Auto-generated method stub
		return super.getDocument(documentId);
	}
	
	@Override
	@GetMapping("/phases")
	@JsonView({JsonSerializationViews.PhaseView.class})
	public Map<String, Object> getDocuments(@RequestParam Map<String, String> filters) {
		// TODO Auto-generated method stub
		return super.getDocuments(filters);
	}
	
	@Override
	@PostMapping("/phases")
	@JsonView({JsonSerializationViews.PhaseForm.class})
	public Phase addDocument(@RequestBody Phase document) throws BusinessException {
		// TODO Auto-generated method stub
		return super.addDocument(document);
	}
	
	@Override
	@PutMapping("/phases/{phase_id}")
	public void updateDocument(@PathVariable("phase_id") Integer documentId, @RequestBody Phase document) throws BusinessException {
		// TODO Auto-generated method stub
		super.updateDocument(documentId, document);
	}
	
	@Override
	@DeleteMapping("/phases/{phase_id}")
	public void deleteDocument(@PathVariable("phase_id") Integer documentId) throws BusinessException {
		// TODO Auto-generated method stub
		super.deleteDocument(documentId);
	}
}