package com.technoliege.appbuilder.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	private JWTUtility jwtUtility;

	public JWTAuthorizationFilter(AuthenticationManager authenticationManager, JWTUtility jwtUtility) {
		super(authenticationManager);
		this.jwtUtility = jwtUtility;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

		String authorizationHeader = request.getHeader(JWTSecurityConstants.HEADER_KEY);
		if (authorizationHeader == null || !authorizationHeader.startsWith(JWTSecurityConstants.TOKEN_PREFIX)) {
			chain.doFilter(request, response);
			return;
		}
		try {
			UsernamePasswordAuthenticationToken authentication = getAuthentication(authorizationHeader);
			SecurityContextHolder.getContext().setAuthentication(authentication);
		} catch (Exception e) {
		}
		chain.doFilter(request, response);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(String authorizationHeader) throws Exception {

		String token = authorizationHeader.replace(JWTSecurityConstants.TOKEN_PREFIX, "");
		UserSecurityDetails userSecurityDetails = jwtUtility.parseJWT(token);
		if (userSecurityDetails != null) {
			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userSecurityDetails.getUsername(), null, userSecurityDetails.getAuthorities());
			usernamePasswordAuthenticationToken.setDetails(userSecurityDetails);
			return usernamePasswordAuthenticationToken;
		} else {
			return null;
		}
	}

}
