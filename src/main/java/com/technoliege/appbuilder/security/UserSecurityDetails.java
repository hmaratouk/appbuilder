package com.technoliege.appbuilder.security;

import java.util.Arrays;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.technoliege.appbuilder.model.UserRole;

public class UserSecurityDetails extends User {
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String name;
	private UserRole role;
	
	public UserSecurityDetails(String username, String password, UserRole role, boolean disabled) {
		super(username, password, !disabled, true, true, true, Arrays.asList(new SimpleGrantedAuthority(role.name())));
		this.role = role;
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserRole getRole() {
		return role;
	}


}
