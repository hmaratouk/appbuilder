package com.technoliege.appbuilder.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.technoliege.appbuilder.model.User;
import com.technoliege.appbuilder.repository.UserRepository;


@Service
public class UserSecurityDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository repository;
	
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = repository.findByUsername(username);
		
		if (user == null) {
			throw new UsernameNotFoundException("User " + username + " not found");
		}
		
		UserSecurityDetails userSecurityDetails = new UserSecurityDetails(user.getUsername(), user.getPassword(), user.getUserRole(), user.isDisabled());
		userSecurityDetails.setId(user.getId());
		userSecurityDetails.setName(user.getFirstName() + " " + user.getLastName());
		
		return userSecurityDetails;
	}

}
