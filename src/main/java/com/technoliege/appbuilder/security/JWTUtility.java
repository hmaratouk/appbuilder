package com.technoliege.appbuilder.security;

import java.util.Date;

import com.technoliege.appbuilder.model.UserRole;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTUtility {


	public String generateJWT(UserSecurityDetails userSecurityDetails) {

		Claims claims = Jwts.claims().setIssuer(JWTSecurityConstants.ISSUER).setIssuedAt(new Date()).setSubject(userSecurityDetails.getUsername()).setExpiration(new Date(new Date().getTime() + JWTSecurityConstants.ACCESS_TOKEN_VALIDITY));
		claims.put(JWTSecurityConstants.USER_ROLE_KEY, userSecurityDetails.getRole());
		claims.put("id", userSecurityDetails.getId());
		claims.put("name", userSecurityDetails.getName());
		claims.put("enabled", userSecurityDetails.isEnabled());
		return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS256, JWTSecurityConstants.SIGNING_KEY).compact();
	}

	public UserSecurityDetails parseJWT(String token) {

		try {
			if (token != null) {
				Claims claims = Jwts.parser().setSigningKey(JWTSecurityConstants.SIGNING_KEY).parseClaimsJws(token).getBody();
				String username = claims.getSubject();
				String userRole = (String) claims.get(JWTSecurityConstants.USER_ROLE_KEY);
				Boolean isEnabled = claims.get("enabled", Boolean.class);
				if (username != null && !username.isEmpty() && userRole != null && !userRole.isEmpty() && isEnabled != null && isEnabled) {
					UserSecurityDetails userSecurityDetails = new UserSecurityDetails(claims.getSubject(), "", UserRole.valueOf((String) claims.get(JWTSecurityConstants.USER_ROLE_KEY)), !claims.get("enabled", Boolean.class));
					userSecurityDetails.setId(claims.get("id", Integer.class));

					return userSecurityDetails;
				}
			}
			return null;
		} catch (Exception e) {
			throw new RuntimeException("Failed to parse JWT due to " + e.getMessage(), e);
		}
	}

}
